<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangKeluarDetailModel extends Model
{
    protected $table = 'barang_keluar_detail';
    protected $fillable = ['id_header', 'sku', 'nama_barang', 'harga', 'jumlah'];
}
