<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangKeluarModel extends Model
{
    protected $table = 'barang_keluar';
    protected $fillable = ['customer', 'no_faktur', 'tanggal', 'total_harga', 'note'];
}
