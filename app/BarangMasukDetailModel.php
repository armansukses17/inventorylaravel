<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangMasukDetailModel extends Model
{
    protected $table = 'barang_masuk_detail';
    protected $fillable = ['id_header', 'sku', 'nama_barang', 'harga', 'jumlah'];
}
