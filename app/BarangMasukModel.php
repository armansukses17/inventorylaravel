<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangMasukModel extends Model
{
    protected $table = 'barang_masuk';
    protected $fillable = ['supplier', 'no_faktur', 'tanggal', 'total_harga', 'note', 'is_active', 'is_delete'];
}
