<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangModel extends Model
{
    protected $table = 'barang';
    protected $fillable = ['id_kategori', 'id_brand', 'id_unit', 'nama_barang', 'sku', 'stok', 'harga_jual', 'desc', 'is_active', 'is_delete'];
}
