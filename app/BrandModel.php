<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandModel extends Model
{
    protected $table = 'master_brand';
    protected $fillable = ['nama_brand', 'kd_brand', 'is_active', 'is_delete'];
}
