<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerModel extends Model
{
    protected $table = 'master_customer';
    protected $fillable = ['customer', 'is_active', 'is_delete'];
}
