<?php

namespace App\Http\Controllers;

use Auth;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
       return view('logins');
    }

    public function postLogin(Request $request)
    {
        // dd($request->only('email', 'password'));
        if (Auth::attempt($request->only('email', 'password'))) {
            return redirect('./');
        } else {
            return redirect()->route('login')->withInput()->withErrors(['msg' => 'Incorrect email or password']);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('logins');
    }
}
