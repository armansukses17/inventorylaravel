<?php

namespace App\Http\Controllers;

use App\BarangModel;
use Illuminate\Http\Request;
use DB;
use DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
// use Input;

class Barang extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $barang = DB::table('barang')->select('id', 'nama_barang')->where('is_active', '1')->get();
        $kategori = DB::table('master_kategori')->select('id', 'nama_kategori', 'kd_kategori')->where('is_active', '1')->get();
        $brand = DB::table('master_brand')->select('id', 'nama_brand', 'kd_brand')->where('is_active', '1')->get();
        $unit = DB::table('master_satuan')->select('id', 'nama_satuan')->where('is_active', '1')->get();
        return view('barang.barang', ['barang' => $barang, 'brand' => $brand, 'kategori' => $kategori, 'unit' => $unit]);
    }

    public function getAutocompleteProduct(Request $request)
    {
        $search = $request->search;
        $barang = DB::table('barang')
                        ->select('id', 
                                'sku', 
                                'nama_barang', 
                                'harga_jual',
                                    DB::raw("(SELECT harga FROM barang_masuk_detail as x
                                    WHERE x.sku = barang.sku
                                    GROUP BY x.id 
                                    ORDER BY x.id DESC
                                    LIMIT 1) as `harga_beli`"))
                        ->where('sku', 'like', '%' . $search . '%')
                        ->orWhere('nama_barang', 'like', '%' .$search . '%')
                        ->orderby('nama_barang', 'asc')
                        ->limit(5)
                        ->get();
        if (count($barang) == 0)
        {
            $response = ['data' => 0, 'm' => 'Item not found !'];
        }
        else
        {
            foreach ($barang as $row) 
            {
                $response[] = [
                    'label' => $row->sku .' - '. $row->nama_barang,
                    'sku' => $row->sku,
                    'item' => $row->nama_barang,  
                    'harga_beli' => $row->harga_beli, 
                    'harga_jual' => $row->harga_jual
                ];
            }
        }
        return response()->json($response);
    }

    public function getAutocompleteProductReadyStock(Request $request)
    {
        $search = $request->search;
        $barang = DB::table('barang')
                        ->select('id', 
                                'sku', 
                                'nama_barang', 
                                'harga_jual',
                                    DB::raw("(SELECT harga FROM barang_masuk_detail as x
                                    WHERE x.sku = barang.sku
                                    GROUP BY x.id 
                                    ORDER BY x.id DESC
                                    LIMIT 1) as `harga_beli`"))
                        ->where(function($query) use ($search) {
                            $query->where('sku', 'like', '%' . $search . '%')
                            ->orWhere('nama_barang', 'like', '%' .$search . '%');
                        })
                        ->where('stok', '>', '0')
                        ->orderby('nama_barang', 'asc')
                        ->limit(5)
                        ->get();
        if (count($barang) == 0)
        {
            $response = ['data' => 0, 'm' => 'Item not found !'];
        }
        else
        {
            foreach ($barang as $row) 
            {
                $response[] = [
                    'label' => $row->sku .' - '. $row->nama_barang,
                    'sku' => $row->sku,
                    'item' => $row->nama_barang,  
                    'harga_beli' => $row->harga_beli, 
                    'harga_jual' => $row->harga_jual
                ];
            }
        }
        return response()->json($response);
    }
    
    public function getDataBarang()
    {
        $data = DB::table('barang as a')
                    ->select('a.id', 'a.sku', 'a.nama_barang', 'b.nama_kategori', 'a.harga_jual', 'a.stok')
                    ->leftJoin('master_kategori as b', 'b.id', '=', 'a.id_kategori')
                    ->where('a.is_active', '1')
                    ->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = '
                        <div class="btn-group">
                            <button class="btn btn-xs btn-info btn-edit" onclick="editBarang('.$row->id.')" title="Detail">
                                <i class="ace-icon fa fa-eye bigger-120"></i>
                            </button>
                        </div>
                        ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function store(Request $request)
    {
        if ($request->ajax()) 
        {
            $rules = array(
                'nama_kategori' => 'required',
                'nama_brand' => 'required',
                'nama_satuan' => 'required',
                'nama_barang' => 'required',
                'harga_jual' => 'required'
            );

            $customMessages = array(
                'required' => 'Kolom :attribute wajib diisi !', // general
            );

            $customFields = array(
                'nama_kategori' => 'Kategori',
                'nama_brand' => 'Brand',
                'nama_satuan' => 'Satuan Barang',
                'nama_barang' => 'Produk',
                'harga_jual' => 'Harga Jual'
            );

            $validator = Validator::make($request->all(), $rules, $customMessages);
            $validator->setAttributeNames($customFields);
            
            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            $getKategori = explode('***', $request->nama_kategori);
            $idKategori = $getKategori[0];
            $kdKategori = $getKategori[1];

            $getBrand = explode('***', $request->nama_brand);
            $idBrand = $getBrand[0];
            $kdBrand = $getBrand[1];
            // SKU
            $sku = substr(date('Y'), 2, 2).$kdKategori.$kdBrand.substr(mt_rand(), 0, 4); // ex:200030025343
            $hargaJual = str_replace('.', '', $request->harga_jual); // hpus titik

            $save = BarangModel::create([
                'id_kategori' => $idKategori,
                'id_brand' => $idBrand,
                'id_unit' => $request->nama_satuan,
                'nama_barang' => $request->nama_barang,
                'sku' => $sku,
                'stok' => 0,
                'harga_jual' => $hargaJual,
                'desc' => isset($request->desc) ? $request->desc : '',
                'is_active' => '1',
                'is_delete' => ''
            ]);

            $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
        }
        return response()->json($return, 200);
    }

    public function edit($id)
    {
        // return $id;
        $getData = BarangModel::where('id', $id)->get();
        // get stockIn
        $getStockIn = DB::table('barang_masuk_detail')->where('sku', $getData[0]->sku)->orderBy('id', 'DESC')->limit(1)->get();
        if (count($getStockIn) > 0)
        {
            $lastPriceIn = $getStockIn[0]->harga;
        }
        else
        {
            $lastPriceIn = 0;
        }
        $return = ['s' => 'success', 'data' => $getData, 'lastPriceStockIn' => $lastPriceIn];
        return response()->json($return, 200);
    }

    public function update(Request $request)
    {
        if ($request->ajax()) 
        {
            $hargaJual = str_replace('.', '', $request->harga_jual_edit); // hpus titik
            $update = BarangModel::where('id', $request->id)->update([
                'id_kategori' => $request->nama_kategori_edit,
                'id_brand' => $request->nama_brand_edit,
                'id_unit' => $request->nama_satuan_edit,
                'nama_barang' => $request->nama_barang_edit,
                'harga_jual' => $hargaJual,
                'desc' => $request->desc_edit
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
        }
        return response()->json($return, 200);
    }

    public function checkStockInWarehouse($sku)
    {
        $data = DB::table('barang')->where('sku', $sku)->get();
        $stock = $data[0]->stok;
        $return = ['s' => 'success', 'lastStock' => $stock];
        return response()->json($return, 200);
    }

    public function checkStockInWarehouse2($sku)
    {
        $inputValue = ($_GET['inputValue']) ? $_GET['inputValue'] : 0;
        $data = DB::table('barang')->where('sku', $sku)->get();
        $stock = $data[0]->stok;
        if ($inputValue > $stock) {
            $lastStock = 'no';
        } else {
            $lastStock = 'yes';
        }
        $return = ['s' => 'success', 'lastStock' => $lastStock];
        return response()->json($return, 200);
    }

    public function destroy($id)
    {
        // return $id;
        // $delete = BarangModel::where('id', $id)->delete();
        $delete = BarangModel::where('id', $id)->update([
            'is_active' => '',
            'is_delete' => '1'
        ]);
        $return = ['s' => 'success', 'm' => 'Data berhasil dihapus'];
        return response()->json($return, 200);
    }
}
