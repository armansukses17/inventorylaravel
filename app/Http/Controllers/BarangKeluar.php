<?php

namespace App\Http\Controllers;

use App\BarangModel;
use App\BarangKeluarModel;
use App\BarangKeluarDetailModel;
use Illuminate\Http\Request;
use DB;
use DataTables;
use Validator;

class BarangKeluar extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $barang = DB::table('barang')->select('id', 'sku', 'nama_barang')->where('is_active', '1')->get();
        $customer = DB::table('master_customer')->select('id', 'customer')->where('is_active', '1')->get();
        return view('barangKeluar.barang-keluar', ['barang' => $barang, 'customer' => $customer]);
    }

    public function generateFaktur(Request $request)
    {
        if ($request->ajax())  
        {
            $numb = '';
            $getData = BarangKeluarModel::select('no_faktur', 'created_at')->orderBy('id', 'desc')->first();
            $rand = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 3);
            $kodeDepan = $rand . date('Ymd');
            if ($getData == null)
            {
                $numb = '00001'; 
            }
            else 
            {
                $getMonth = substr($getData->created_at, 5, 2);
                $getLastFaktur = substr($getData->no_faktur, -5);
                $getLastFaktur++;

                if (date('m') > $getMonth) // mulai lg dr nol
                {
                    $numb = '00001'; 
                }
                else
                {
                    if ($getLastFaktur <= 99999)
                    {
                        $numb = sprintf("%05s", $getLastFaktur);
                    }
                } 
            }
            $noFaktur = $kodeDepan . $numb;
        }
        $return = ['s' => 'success', 'noFaktur' => $noFaktur];
        return response()->json($return, 200);
    }

    public function getDataBarangKeluar()
    {
        $data = DB::table('barang_keluar as a')
                    ->select('a.id', 'a.customer', 'a.no_faktur', 'a.tanggal', 'a.total_harga', 'a.note')
                    ->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = '
                        <div class="btn-group">
                            <button class="btn btn-xs btn-info btn-edit" onclick="detailBarangKeluar('.$row->id.')" title="Detail">
                                <i class="ace-icon fa fa-eye bigger-120"></i>
                            </button>
                        </div>
                        ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function store(Request $request)
    {
        if ($request->ajax()) 
        {
            try {
                // ----------------------------------
                DB::beginTransaction();
                // ----------------------------------
                $rules = array(
                    'date_in' => 'required',
                    'customer' => 'required',
                    'no_faktur' => 'required',
                    'myQty.*' => 'required',
                    'myPurchasePrice.*' => 'required'
                );
                $customMessages = array(
                    'required' => '- :attribute field is required !'
                );
                $customFields = array(
                    'date_in' => 'Date',
                    'customer' => 'customer',
                    'no_faktur' => 'No. Faktur',
                    'myQty.*' => 'Qty',
                    'myPurchasePrice.*' => 'Purchase Price'
                );

                $validator = Validator::make($request->all(), $rules, $customMessages);
                $validator->setAttributeNames($customFields);
                
                if ($validator->fails()) {
                    return response()->json(['errors'=>$validator->errors()->all()]);
                }
                // -------------------------------------------------------
                $listItem = isset($request->mySku) ? $request->mySku : 0;

                if ($listItem == 0) {
                    $return = ['s' => 'fail', 'm' => 'Silahkan tambah item terlebih dahulu ! !'];
                }
                else
                {
                    $header = [
                        'customer' => $request->customer,
                        'no_faktur' => $request->no_faktur,
                        'tanggal' => $request->date_in,
                        'total_harga' => $request->total_harga,
                        'note' => ($request->note) ? $request->note : ''
                    ];
                    // Insert
                    $insertHeader = BarangKeluarModel::create($header);
                    $idHeader = $insertHeader->id;

                    for ($i = 0; $i < count($request->mySku); $i++) 
                    {
                        if ($request->myPurchasePrice[$i] == 0) 
                        {
                            return response()->json(['s' => 'fail', 'm' => 'Harga jual tidak boleh kosong !']);
                            die;
                        }

                        $dataArray[] = [
                            'id_header' => $idHeader,
                            'sku' => $request->mySku[$i],
                            'nama_barang' => $request->myItem[$i],
                            'harga' => $request->myPurchasePrice[$i],
                            'jumlah' => $request->myQty[$i],
                            'created_at' => \Carbon\Carbon::now()->toDateTimeString(), // untuk mengisi field ini ketika insert array
                            'updated_at' => \Carbon\Carbon::now()->toDateTimeString() // untuk mengisi field ini ketika insert array
                        ];
                        // ------------------------------------------------------------------------------------------------
                        $getLastStock = BarangModel::where('sku', $request->mySku[$i])->orderBy('id', 'DESC')->limit(1)->get();
                        $lastStock = (count($getLastStock) > 0) ? $getLastStock[0]->stok : 0;

                        // update product
                        BarangModel::where('sku', $request->mySku[$i])->update([
                            'stok' => $lastStock - $request->myQty[$i]
                        ]);
                    }
                    BarangKeluarDetailModel::insert($dataArray);
                    $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
                }
                // ----------------------------------------------
                DB::commit();
                // ----------------------------------------------
                return response()->json($return);
            } catch (\Throwable $e) {
                // ----------------------------------------------
                DB::rollback();
                // ----------------------------------------------
                throw $e;
            } 
        }
    }

    public function edit($id)
    {
        $header = DB::table('barang_keluar')->select('id as idKeluar', 'customer', 'no_faktur', 'tanggal', 'total_harga', 'note')->where('id', '=', $id)->first();
        $detail = DB::table('barang_keluar_detail')->where('id_header', '=', $id)->get();
        $data = array_merge(
            ['header' => $header], 
            ['detail' => json_decode($detail)]
        );
        $return = ['s' => 'success', 'respon' => $data];
        return response()->json($return, 200);
    }
}
