<?php

namespace App\Http\Controllers;

use App\BarangModel;
use App\BarangMasukModel;
use App\BarangMasukDetailModel;
use Illuminate\Http\Request;
use DB;
use DataTables;
use Validator;

class BarangMasuk extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $barang = DB::table('barang')->select('id', 'sku', 'nama_barang')->where('is_active', '1')->get();
        $supplier = DB::table('master_supplier')->select('id', 'nama_supplier')->where('is_active', '1')->get();
        return view('barangMasuk.barang-masuk', ['barang' => $barang, 'supplier' => $supplier]);
    }

    public function getDataBarangMasuk()
    {
        $data = DB::table('barang_masuk as a')
                    ->select('a.id', 'a.supplier', 'a.no_faktur', 'a.tanggal', 'a.total_harga', 'a.note')
                    ->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = '
                        <div class="btn-group">
                            <button class="btn btn-xs btn-info btn-edit" onclick="detailBarangMasuk('.$row->id.')" title="Detail">
                                <i class="ace-icon fa fa-eye bigger-120"></i>
                            </button>
                        </div>
                        ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function store(Request $request)
    {
        if ($request->ajax()) 
        {
            try {
                // ----------------------------------
                DB::beginTransaction();
                // ----------------------------------
                $rules = array(
                    'date_in' => 'required',
                    'supplier' => 'required',
                    'no_faktur' => 'required',
                    'myQty.*' => 'required',
                    'myPurchasePrice.*' => 'required'
                );
                $customMessages = array(
                    'required' => '- :attribute field is required !'
                );
                $customFields = array(
                    'date_in' => 'Date',
                    'supplier' => 'Supplier',
                    'no_faktur' => 'No. Faktur',
                    'myQty.*' => 'Qty',
                    'myPurchasePrice.*' => 'Purchase Price'
                );

                $validator = Validator::make($request->all(), $rules, $customMessages);
                $validator->setAttributeNames($customFields);
                
                if ($validator->fails()) {
                    return response()->json(['errors'=>$validator->errors()->all()]);
                }
                // -------------------------------------------------------
                $listItem = isset($request->mySku) ? $request->mySku : 0;

                if ($listItem == 0) {
                    $return = ['s' => 'fail', 'm' => 'Silahkan tambah item terlebih dahulu ! !'];
                }
                else
                {
                    $header = [
                        'supplier' => $request->supplier,
                        'no_faktur' => $request->no_faktur,
                        'tanggal' => $request->date_in,
                        'total_harga' => $request->total_harga,
                        'note' => ($request->note) ? $request->note : ''
                    ];
                    // Insert
                    $insertHeader = BarangMasukModel::create($header);
                    $idHeader = $insertHeader->id;

                    for ($i = 0; $i < count($request->mySku); $i++) 
                    {
                        if ($request->myPurchasePrice[$i] == 0) 
                        {
                            return response()->json(['s' => 'fail', 'm' => 'Harga beli tidak boleh kosong !']);
                            die;
                        }

                        $dataArray[] = [
                            'id_header' => $idHeader,
                            'sku' => $request->mySku[$i],
                            'nama_barang' => $request->myItem[$i],
                            'harga' => $request->myPurchasePrice[$i],
                            'jumlah' => $request->myQty[$i],
                            'created_at' => \Carbon\Carbon::now()->toDateTimeString(), // untuk mengisi field ini ketika insert array
                            'updated_at' => \Carbon\Carbon::now()->toDateTimeString() // untuk mengisi field ini ketika insert array
                        ];
                        // ------------------------------------------------------------------------------------------------
                        $getLastStock = BarangModel::where('sku', $request->mySku[$i])->orderBy('id', 'DESC')->limit(1)->get();
                        $lastStock = (count($getLastStock) > 0) ? $getLastStock[0]->stok : 0;

                        // update product
                        BarangModel::where('sku', $request->mySku[$i])->update([
                            'stok' => $lastStock + $request->myQty[$i] // stok terakhir + stok masuk
                        ]);
                    }
                    BarangMasukDetailModel::insert($dataArray);
                    $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
                }
                // ----------------------------------------------
                DB::commit();
                // ----------------------------------------------
                return response()->json($return);
            } catch (\Throwable $e) {
                // ----------------------------------------------
                DB::rollback();
                // ----------------------------------------------
                throw $e;
            } 
        }
    }

    public function edit($id)
    {
        $header = DB::table('barang_masuk')->select('id as idMasuk', 'supplier', 'no_faktur', 'tanggal', 'total_harga', 'note')->where('id', '=', $id)->first();
        $detail = DB::table('barang_masuk_detail')->where('id_header', '=', $id)->get();
        $data = array_merge(
            ['header' => $header], 
            ['detail' => json_decode($detail)]
        );
        $return = ['s' => 'success', 'respon' => $data];
        return response()->json($return, 200);
    }
}
