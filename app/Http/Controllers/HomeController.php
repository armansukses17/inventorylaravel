<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $thisDay = date('Ymd');
        $total_barang = count(DB::table('barang')->select('id')->get());
        $total_supplier = count(DB::table('master_supplier')->select('id')->get());
        $total_customer = count(DB::table('master_customer')->select('id')->get());
        $in = DB::table('barang')
                ->select(
                    DB::raw("SUM((SELECT sum(n.jumlah) FROM barang_masuk_detail n 
                        LEFT JOIN barang_masuk m ON m.id = n.id_header
                        WHERE n.sku = barang.sku 
                        AND DATE_FORMAT(m.tanggal, '%Y%m%d') = '$thisDay'
                        GROUP BY n.sku
                        LIMIT 1)) as `jml_masuk`"
                    )
                )
                ->get();
        $countIn = isset($in[0]->jml_masuk) ? $in[0]->jml_masuk : 0;

        $out = DB::table('barang')
                ->select(
                    DB::raw("SUM((SELECT sum(n.jumlah) FROM barang_keluar_detail n 
                        LEFT JOIN barang_keluar m ON m.id = n.id_header
                        WHERE n.sku = barang.sku 
                        AND DATE_FORMAT(m.tanggal, '%Y%m%d') = '$thisDay'
                        GROUP BY n.sku
                        LIMIT 1)) as `jml_keluar`"
                    )
                )
                ->get();
        $countOut = isset($out[0]->jml_keluar) ? $out[0]->jml_keluar : 0;

        $data = [
            'jumlah_masuk_hari_ini' => $countIn,
            'jumlah_keluar_hari_ini' => $countOut,
            'total_barang' => $total_barang,
            'total_supplier' => $total_supplier,
            'total_customer' => $total_customer
        ];

        $pieChart = $this->pieChart();
        // print_r($data); die;
        return view('dashboard', ['total' => $data, 'pieChart' => $pieChart]);
    }

    public function pieChart()
	{
        $year = date('Y');
        $in = DB::table('barang')
                ->select(
                    DB::raw("SUM((SELECT sum(n.jumlah) FROM barang_masuk_detail n 
                        LEFT JOIN barang_masuk m ON m.id = n.id_header
                        WHERE n.sku = barang.sku 
                        AND DATE_FORMAT(m.tanggal, '%Y') = '$year'
                        GROUP BY n.sku
                        LIMIT 1)) as `jml_masuk`"
                    )
                )
                ->get();
        $countIn = isset($in[0]->jml_masuk) ? $in[0]->jml_masuk : 0;

        $out = DB::table('barang')
                ->select(
                    DB::raw("SUM((SELECT sum(n.jumlah) FROM barang_keluar_detail n 
                        LEFT JOIN barang_keluar m ON m.id = n.id_header
                        WHERE n.sku = barang.sku 
                        AND DATE_FORMAT(m.tanggal, '%Y') = '$year'
                        GROUP BY n.sku
                        LIMIT 1)) as `jml_keluar`"
                    )
                )
                ->get();
        $countOut = isset($out[0]->jml_keluar) ? $out[0]->jml_keluar : 0;

		$stringJson = "
            [
                { name: 'Barang Keluar', y: $countOut },
                { name: 'Barang Masuk', y: $countIn }
            ]
        ";
		return $stringJson;
	}
}
