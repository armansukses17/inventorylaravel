<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTables;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class LaporanStok extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function pageAllReport()
    {
        $data = [];
        return view('laporan.stokBarang', $data);
    }

    public function getLapAllStok()
    {
        $filterType = isset($_GET['filterType3']) ? $_GET['filterType3'] : '';
        $filterBy = isset($_GET['filterBy3']) ? $_GET['filterBy3'] : '';
        $yearWithMonth = isset($_GET['yearWithMonth3']) ? $_GET['yearWithMonth3'] : '';
        $format = '';
         
        if (empty($filterBy)) 
        {
            $format = 'notFound!';
            $where = "---";
        } 
        else 
        {
            if ($filterType == 1) 
            {
                $format = '%Y%m';
                $where = $yearWithMonth.$filterBy;
            }
            elseif ($filterType == 2) 
            {
                $format = '%Y';
                $where = $filterBy;
            }
        }

        $data = DB::table('barang')
            ->select(
                'nama_barang',
                'sku',
                DB::raw("(SELECT sum(n.jumlah) FROM barang_masuk_detail n 
                    LEFT JOIN barang_masuk m ON m.id = n.id_header
                    WHERE n.sku = barang.sku 
                    AND DATE_FORMAT(m.tanggal, '$format') = '$where'
                    GROUP BY n.sku
                    LIMIT 1) as `jml_masuk`"),
                DB::raw("(SELECT sum(q.jumlah) FROM barang_keluar_detail q 
                    LEFT JOIN barang_keluar r ON r.id = q.id_header
                    WHERE q.sku = barang.sku 
                    AND DATE_FORMAT(r.tanggal, '$format') = '$where'
                    GROUP BY q.sku
                    LIMIT 1) as `jml_keluar`")
            )
            ->get();
        $return = ['s' => 'success', 'm' => $data];
        return response()->json($return, 200);
    }

    public function testDownlod()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello World !');

        $writer = new Xlsx($spreadsheet);
        $thisDay = date('Y-m-d--His');
        $filename = "adadasd#$thisDay";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file 
    }

    public function LaporanStockBarang(Request $request)
    {
        $a = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        $b = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $c = array_combine($a, $b);
        // ----------------------------------------

        $filterType = isset($_GET['filterType3']) ? $_GET['filterType3'] : '';
        $filterBy = isset($_GET['filterBy3']) ? $_GET['filterBy3'] : '';
        $yearWithMonth = isset($_GET['yearWithMonth3']) ? $_GET['yearWithMonth3'] : '';
        $format = '';
         
        if (empty($filterBy)) 
        {
            $format = 'notFound!';
            $where = "---";
            $title = "DATA NOT FOUND !";
        } 
        else 
        {
            if ($filterType == 1) 
            {
                $format = '%Y%m';
                $where = $yearWithMonth.$filterBy;
                $mountPeriode = !empty($filterBy) ? strtoupper($c[$filterBy]) : '';
                $title = "LAPORAN STOK BARANG PER BULAN $mountPeriode $yearWithMonth";
            }
            elseif ($filterType == 2) 
            {
                $format = '%Y';
                $where = $filterBy;
                $title = "LAPORAN STOK BARANG PER TAHUN $filterBy";
            }
        }
        // --------------------------------------
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // header utama
        $sheet->setCellValue('A1', $title)->mergeCells('A1:D1');
        $sheet->setCellValue('A2', "PT. GARMEN SUKSES MAKMUR SENTOSA")->mergeCells('A2:D2');
        // header table
        $sheet->setCellValue('A4', 'SKU')->getColumnDimension('A')->setWidth(20);
        $sheet->setCellValue('B4', 'NAMA BARANG')->getColumnDimension('B')->setWidth(50);
        $sheet->setCellValue('C4', 'JUMLAH MASUK')->getColumnDimension('C')->setWidth(20);
        $sheet->setCellValue('D4', 'JUMLAH KELUAR')->getColumnDimension('D')->setWidth(20);

        $data = DB::table('barang')
                    ->select(
                        'sku',
                        'nama_barang',
                        DB::raw("(SELECT sum(n.jumlah) FROM barang_masuk_detail n 
                            LEFT JOIN barang_masuk m ON m.id = n.id_header
                            WHERE n.sku = barang.sku 
                            AND DATE_FORMAT(m.tanggal, '$format') = '$where'
                            GROUP BY n.sku
                            LIMIT 1) as `jml_masuk`"),
                        DB::raw("(SELECT sum(q.jumlah) FROM barang_keluar_detail q 
                            LEFT JOIN barang_keluar r ON r.id = q.id_header
                            WHERE q.sku = barang.sku 
                            AND DATE_FORMAT(r.tanggal, '$format') = '$where'
                            GROUP BY q.sku
                            LIMIT 1) as `jml_keluar`")
                    )
                    ->get();

        $resultArray = json_decode(json_encode($data), true); // get result_array in laravel
        $countSheet1 = 4 + count($resultArray);
        $sheet->fromArray(
            $resultArray,
            null,
            'A5'
        );
        // ------------------------------------------

        $styleTitleHeader1 = [
            'font' => [
                'bold' => TRUE,
                'size' => 14
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
        ];

        $styleTitleHeader2 = [
            'font' => [
                'bold' => TRUE,
                'size' => 20,
                'color' => [ 'rgb' => '438EB9' ]
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ],
        ];

        // style font header table
        $styleTitleHeader = [
            'font' => [
                'bold' => TRUE,
                'size' => 11,
                'color' => [ 'rgb' => 'FFFFFF' ]
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ];

        // all border table
        $borderAllContent = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
                ],
            ]
        ];

        $sheet->getStyle('A1:D1')->applyFromArray($styleTitleHeader1);
        $sheet->getStyle('A2:D2')->applyFromArray($styleTitleHeader2);
        $sheet->getStyle('A4:D4')->applyFromArray($styleTitleHeader);
        $sheet->getStyle('A4:D4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('3A8659');
        $sheet->getStyle("A4:D$countSheet1")->applyFromArray($borderAllContent);
        
        // type column string
        for ($i = 0; $i < count($resultArray); $i++) {
            $row = $i+5;  
            $sheet->setCellValueExplicit('A'.$row, $resultArray[$i]['sku'],\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        }

        // ----------------------------

        $writer = new Xlsx($spreadsheet);
        $date = date('YmdHis');
        $filename = "stockBarang#$date";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
        header('Cache-Control: max-age=0');
        $writer->save('php://output'); // download file 
    }
}
