<?php

namespace App\Http\Controllers;

use App\BrandModel;
use Illuminate\Http\Request;
use DB;
use DataTables;

class MasterBrand extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('masterBrand.brand');
    }

    public function getDataBrand()
    {
        $data = DB::table('master_brand')->select('id', 'nama_brand')->where('is_active', '1')->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = '
                        <div class="btn-group">
                            <button class="btn btn-xs btn-info btn-edit" onclick="editBrand('.$row->id.')">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </button>
                        </div>
                        ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function store(Request $request)
    {
        if ($request->ajax()) 
        {
            $validation = $request->validate([
                'nama_brand' => 'required'
            ]);

            $getData = BrandModel::select('id', 'kd_brand')->orderBy('kd_brand', 'DESC')->limit(1)->get();
            $countData = count($getData);
            if ($countData == 0)
            {
                $code = '001';
            } 
            else
            {
                $lastCode = $getData[0]->kd_brand;
                $lastCode++;
                $code = sprintf("%03s", $lastCode);
            }
            BrandModel::create([
                'nama_brand' => $request->nama_brand,
                'kd_brand' => $code,
                'is_active' => '1',
                'is_delete' => ''
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
            return response()->json($return, 200);
        }
    }

    public function edit($id)
    {
        // return $id;
        $getData = BrandModel::where('id', $id)->get();
        $return = ['s' => 'success', 'data' => $getData];
        return response()->json($return, 200);
    }

    public function update(Request $request)
    {
        if ($request->ajax()) 
        {
            $update = BrandModel::where('id', $request->id)->update([
                'nama_brand' => $request->nama_brand_edit
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
            return response()->json($return, 200);
        }
    }
    
    public function destroy($id)
    {
        $delete = BrandModel::where('id', $id)->update([
            'is_active' => '',
            'is_delete' => '1'
        ]);
        $return = ['s' => 'success', 'm' => 'Data berhasil dihapus'];
        return response()->json($return, 200);
    }
}
