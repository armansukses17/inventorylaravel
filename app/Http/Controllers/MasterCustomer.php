<?php

namespace App\Http\Controllers;

use App\CustomerModel;
use Illuminate\Http\Request;
use DB;
use DataTables;

class MasterCustomer extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('masterCustomer.customer');
    }

    public function getDataCustomer()
    {
        $data = DB::table('master_customer')->select('id', 'customer')->where('is_active', '1')->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = '
                        <div class="btn-group">
                            <button class="btn btn-xs btn-info btn-edit" onclick="editCustomer('.$row->id.')">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </button>
                        </div>
                        ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function store(Request $request)
    {
        if ($request->ajax()) 
        {
            $validation = $request->validate([
                'customer' => 'required'
            ]);
            $save = CustomerModel::create([
                'customer' => $request->customer,
                'is_active' => '1',
                'is_delete' => ''
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
            return response()->json($return, 200);
        }
    }

    public function edit($id)
    {
        $getData = CustomerModel::where('id', $id)->get();
        $return = ['s' => 'success', 'data' => $getData];
        return response()->json($return, 200);
    }

    public function update(Request $request)
    {
        if ($request->ajax()) 
        {
            $update = CustomerModel::where('id', $request->id)->update([
                'customer' => $request->customer_edit
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
            return response()->json($return, 200);
        }
    }

    public function destroy($id)
    {
        $delete = CustomerModel::where('id', $id)->update([
            'is_active' => '',
            'is_delete' => '1'
        ]);
        $return = ['s' => 'success', 'm' => 'Data berhasil dihapus'];
        return response()->json($return, 200);
    }
}
