<?php

namespace App\Http\Controllers;

use App\KategoriModel;
use Illuminate\Http\Request;
use DB;
use DataTables;

class MasterKategori extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('masterKategori.kategori');
    }

    public function getDataKategori()
    {
        $data = DB::table('master_kategori')->select('id', 'nama_kategori')->where('is_active', '1')->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = '
                        <div class="btn-group">
                            <button class="btn btn-xs btn-info btn-edit" onclick="editKategori('.$row->id.')">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </button>
                        </div>
                        ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function store(Request $request)
    {
        if ($request->ajax()) 
        {
            $validation = $request->validate([
                'nama_kategori' => 'required'
            ]);
            $getData = KategoriModel::select('id', 'kd_kategori')->orderBy('kd_kategori', 'DESC')->limit(1)->get();
            $countData = count($getData);
            if ($countData == 0)
            {
                $code = '001';
            } 
            else
            {
                $lastCode = $getData[0]->kd_kategori;
                $lastCode++;
                $code = sprintf("%03s", $lastCode);
            }
            KategoriModel::create([
                'nama_kategori' => $request->nama_kategori,
                'kd_kategori' => $code,
                'is_active' => '1',
                'is_delete' => ''
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
            return response()->json($return, 200);
        }
    }

    public function edit($id)
    {
        $getData = KategoriModel::where('id', $id)->get();
        $return = ['s' => 'success', 'data' => $getData];
        return response()->json($return, 200);
    }

    public function update(Request $request)
    {
        if ($request->ajax()) 
        {
            $update = KategoriModel::where('id', $request->id)->update([
                'nama_kategori' => $request->nama_kategori_edit
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
            return response()->json($return, 200);
        }
    }

    public function destroy($id)
    {
        // return $id;
        // $delete = KategoriModel::where('id', $id)->delete();
        $delete = KategoriModel::where('id', $id)->update([
            'is_active' => '',
            'is_delete' => '1'
        ]);
        $return = ['s' => 'success', 'm' => 'Data berhasil dihapus'];
        return response()->json($return, 200);
    }
}
