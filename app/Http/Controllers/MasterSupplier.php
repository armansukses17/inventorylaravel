<?php

namespace App\Http\Controllers;

use App\SupplierModel;
use Illuminate\Http\Request;
use DB;
use DataTables;

class MasterSupplier extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('masterSupplier.supplier');
    }

    public function getDataSupplier()
    {
        $data = DB::table('master_supplier')->select('id', 'nama_supplier')->where('is_active', '1')->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = '
                        <div class="btn-group">
                            <button class="btn btn-xs btn-info btn-edit" onclick="editSupplier('.$row->id.')">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </button>
                        </div>
                        ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function store(Request $request)
    {
        if ($request->ajax()) 
        {
            $validation = $request->validate([
                'nama_supplier' => 'required'
            ]);
            $save = SupplierModel::create([
                'nama_supplier' => $request->nama_supplier,
                'is_active' => '1',
                'is_delete' => ''
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
            return response()->json($return, 200);
        }
    }

    public function edit($id)
    {
        $getData = SupplierModel::where('id', $id)->get();
        $return = ['s' => 'success', 'data' => $getData];
        return response()->json($return, 200);
    }

    public function update(Request $request)
    {
        if ($request->ajax()) 
        {
            $update = SupplierModel::where('id', $request->id)->update([
                'nama_supplier' => $request->nama_supplier_edit
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
            return response()->json($return, 200);
        }
    }

    public function destroy($id)
    {
        $delete = SupplierModel::where('id', $id)->update([
            'is_active' => '',
            'is_delete' => '1'
        ]);
        $return = ['s' => 'success', 'm' => 'Data berhasil dihapus'];
        return response()->json($return, 200);
    }
}
