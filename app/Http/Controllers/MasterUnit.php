<?php

namespace App\Http\Controllers;

use App\UnitModel;
use Illuminate\Http\Request;
use DB;
use DataTables;

class MasterUnit extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $data = DB::table('master_satuan')->select('id', 'nama_satuan')->where('is_active', '1')->get();
        return view('masterUnit.unit'); // ['data' => $data]
    }

    public function getDataUnit()
    {
        $data = DB::table('master_satuan')->select('id', 'nama_satuan')->where('is_active', '1')->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = '
                        <div class="btn-group">
                            <button class="btn btn-xs btn-info btn-edit" onclick="editUnit('.$row->id.')">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </button>
                        </div>
                        ';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        $data = 'Tambah Data';
        return view('createunit', ['title' => $data]);
    }

    public function store(Request $request)
    {
        if ($request->ajax()) 
        {
            $validation = $request->validate([
                'nama_satuan' => 'required'
            ]);
            
            $save = UnitModel::create([
                'nama_satuan' => $request->nama_satuan,
                'is_active' => '1',
                'is_delete' => ''
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
            return response()->json($return, 200);
        }
    }

    public function edit($id)
    {
        $getData = UnitModel::where('id', $id)->get();
        $return = ['s' => 'success', 'data' => $getData];
        return response()->json($return, 200);
    }

    public function update(Request $request)
    {
        if ($request->ajax()) 
        {
            $update = UnitModel::where('id', $request->id)->update([
                'nama_satuan' => $request->nama_satuan_edit
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
            return response()->json($return, 200);
        }
    }

    public function destroy($id)
    {
        UnitModel::where('id', $id)->update([
            'is_active' => '',
            'is_delete' => '1'
        ]);
        $return = ['s' => 'success', 'm' => 'Data berhasil dihapus'];
        return response()->json($return, 200);
    }
}
