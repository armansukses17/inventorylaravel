<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use DB;
use DataTables;
use Validator;

class MasterUser extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('user.user');
    }

    public function getDataUser()
    {
        $data = DB::table('users')
                    ->select('id', 
                            'photo', 
                            'fullName', 
                            'email', 
                            DB::raw('"**********" as password'), 
                            DB::raw('(CASE WHEN level = 1 THEN "Owner" ELSE "Admin" END) AS level'), 
                            'phone')
                    ->where('is_active', '1')
                    ->get(); // query builder

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row) {
                $btn = '<div class="btn-group">
                            <a href="/masterUser/detail/'.$row->id.'" class="btn btn-xs btn-success btn-detail" title="Detail">
                                <i class="ace-icon fa fa-eye bigger-120"></i>
                            </a>
                            <button class="btn btn-xs btn-danger" onclick="deleteUser('.$row->id.')">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </button> 
                        </div>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function store(Request $request)
    {
        if ($request->ajax()) 
        {
            // VALIDATION
            $rules = array(
                'fullName' => 'required',
                'gender' => 'required',
                'email' => 'required',
                'password' => 'required',
                'role' => 'required',
                'telp' => 'required',
                'address' => 'required',
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            );

            $customMessages = array(
                'required' => 'Kolom :attribute wajib diisi !', // general
                // 'fullName.required' => 'Kolom :attribute blablabla', // contoh customs message
            );

            $customFields = array(
                'fullName' => 'Nama Lengkap',
                'gender' => 'jenis Kelamin',
                'telp' => 'No. Telepon',
                'address' => 'Alamat',
                'photo' => 'Foto'
            );

            $validator = Validator::make($request->all(), $rules, $customMessages);
            $validator->setAttributeNames($customFields);
            
            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }
            // END: VALIDATION

            if ($request->file('photo')) {
                $imagePath = $request->file('photo');
                $imageName = date('YmdHis').$imagePath->getClientOriginalName(); 
                $path = $imagePath->move('assets/files/users', $imageName);
            }

            $save = Users::create([
                'fullName' => $request->fullName,
                'gender' => $request->gender,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'level' => $request->role,
                'phone' => $request->telp,
                'address' => $request->address,
                'photo' => $imageName,
                'is_active' => '1',
                'is_delete' => ''
            ]);
            $return = ['s' => 'success', 'm' => 'Data berhasil disimpan !'];
            return response()->json($return, 200);
        }
    }

    public function show($id)
    {
        $getData = Users::where('id', $id)->first();
        return view('user.userDetail', ['detail' => $getData]);
    }

    public function edit($id)
    {
        $getData = Users::where('id', $id)->get();
        $return = ['s' => 'success', 'data' => $getData];
        return response()->json($return, 200);
    }

    public function update(Request $request)
    {
        if ($request->ajax()) 
        {
            if ($request->file('photo_edit')) 
            {
                $validation = $request->validate([
                    'fullName_edit' => 'required',
                    'gender_edit' => 'required',
                    'email_edit' => 'required',
                    'telp_edit' => 'required',
                    'address_edit' => 'required',
                    'photo_edit' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                
                // EDIT PHOTO ---
                $getUser = Users::find($request->id);
                $path = public_path().'/assets/files/users/';

                // remove old file
                if (file_exists($path.$getUser->photo) && ($getUser->photo != '' OR $getUser->photo != null)) {
                   $file_old = $path.$getUser->photo;
                   unlink($file_old);
                }

                // upload new file
                $file = $request->file('photo_edit');
                $imageName = date('YmdHis').$file->getClientOriginalName();
                $file->move($path, $imageName);
                // END: EDIT PHOTO ---

                $update = Users::where('id', $request->id)->update([
                    'fullName' => $request->fullName_edit,
                    'gender' => $request->gender_edit,
                    'email' => $request->email_edit,
                    'phone' => $request->telp_edit,
                    'address' => $request->address_edit,
                    'photo' => $imageName
                ]);
            }
            else 
            {
                $validation = $request->validate([
                    'fullName_edit' => 'required',
                    'gender_edit' => 'required',
                    'email_edit' => 'required',
                    'telp_edit' => 'required',
                    'address_edit' => 'required'
                ]);
                
                $update = Users::where('id', $request->id)->update([
                    'fullName' => $request->fullName_edit,
                    'gender' => $request->gender_edit,
                    'email' => $request->email_edit,
                    'phone' => $request->telp_edit,
                    'address' => $request->address_edit
                ]);
            }
            $return = ['s' => 'success', 'm' => 'Data berhasil diperbarui !'];
            return response()->json($return, 200);
        }
    }

    public function destroy($id)
    {
        $delete = Users::where('id', $id)->update([
            'is_active' => '',
            'is_delete' => '1'
        ]);
        $return = ['s' => 'success', 'm' => 'Data berhasil dihapus'];
        return response()->json($return, 200);
    }
}
