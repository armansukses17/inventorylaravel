<?php
// http://localhost:8085/api/user
// http://localhost:8085/api/user/show/2
// etc
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;

class RestApiUser extends Controller
{
    public function index()
    {
        // return Users::all();
        $data = Users::all();
        return response()->json($data);
    }

    public function create(request $request)
    {
        $data = new Users();
        $data->fullName = $request->fullName;
        $data->gender = $request->gender;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->address = $request->address;

        $save = $data->save();
        $resp = ['s' => 'success', 'msg' => 'Data berhasil disimpan !'];
        return $resp;
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $getData = Users::where('id', $id)->get();
        return $getData;
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $data = Users::find($id);
        $data->fullName = $request->fullName;
        $data->gender = $request->gender;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->address = $request->address;
        $update = $data->save();
        $resp = ['s' => 'success', 'msg' => 'Data berhasil diperbarui !'];
        return $resp;
    }

    public function destroy($id)
    {
        $delete = Users::find($id);
        $delete = $delete->delete();
        $resp = ['s' => 'success', 'msg' => 'Data berhasil dihapus !'];
        return $resp;
    }
}
