<?php

namespace App\Http\Middleware;

use Closure;

class CheckRolle
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if (in_array(auth()->user()->level, $roles)) { // multi level
            return $next($request);
        }
   
        return redirect('/logins')->with('error', "You don't have admin access.");
    }
}
