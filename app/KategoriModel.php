<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriModel extends Model
{
    protected $table = 'master_kategori';
    protected $fillable = ['nama_kategori', 'kd_kategori', 'is_active', 'is_delete'];
}
