<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierModel extends Model
{
    protected $table = 'master_supplier';
    protected $fillable = ['nama_supplier', 'is_active', 'is_delete'];
}
