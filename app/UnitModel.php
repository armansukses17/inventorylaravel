<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitModel extends Model
{
    protected $table = 'master_satuan';
    protected $fillable = ['nama_satuan', 'is_active', 'is_delete'];
    // public $timestamps = true;
}
