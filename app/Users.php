<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

// class Users extends Model
class Users extends Authenticatable
{
    // protected $table = 'users';
    protected $fillable = ['fullName', 'gender', 'email', 'password', 'level', 'photo', 'phone', 'address', 'is_active', 'is_delete'];
}
