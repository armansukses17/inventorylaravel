@extends('master')

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="{{ url('./') }}">Home</a>
                </li>
                <li class="active">Data Barang</li>
            </ul><!-- /.breadcrumb -->
        </div>
        
        <!-- ISI CONTENT -->
        <div class="page-content">
            
            @include('includes.setting-template')

            <div class="page-header">
                <h1>
                   Data Barang
                </h1>
            </div><!-- /.page-header -->
            <br>

            <div class="row">
                <div class="col-xs-12">

                    <a href="#modalAddData" role="button" class="btn btn-danger btn-sm btn-add" data-toggle="modal"> Add Data </a>
                    <!-- <a href="#modalAddStok" role="button" class="btn btn-success btn-sm btn-add-stock" data-toggle="modal"> Add Stock </a> -->

                    <!-- <div class="hr hr-18 dotted hr-double"></div> -->
                    <br><br>

                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table class="table table-hover" id="tblData">
                               <thead>
                                    <tr>
                                        <th width="20px">No</th>
                                        <th>Sku</th>
                                        <th>Nama Barang</th>
                                        <th>Kategori</th>
                                        <th>Stok</th>
                                        <th>Harga Jual</th>
                                        <th width="80px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="modalAddData" class="modal" tabindex="-1" data-keyboard="false" data-backdrop="static">
                        <div class="modal-dialog swing">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="blue bigger">Tambah Barang</h4>
                                </div>

                                <div class="msgAlert"></div>

                                <form id="addMasterBarang"> <!-- method="post" action="saveBarang" -->
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label>Nama Barang</label>
                                                    <input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Nama barang ..">
                                                </div>
                                                <div class="form-group">
                                                    <label>Kategori</label>
                                                    <select class="chosen-select form-control" name="nama_kategori" id="nama_kategori">
                                                        <option value="">-- Pilih Kategori --</option>
                                                        @foreach($kategori as $val)
                                                            <option value="{{ $val->id .'***'. $val->kd_kategori }}">{{ $val->nama_kategori }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Brand</label>
                                                    <select class="chosen-select form-control" name="nama_brand" id="nama_brand">
                                                        <option value="">-- Pilih Brand --</option>
                                                        @foreach($brand as $val)
                                                            <option value="{{ $val->id .'***'. $val->kd_brand }}">{{ $val->nama_brand }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Satuan Barang</label>
                                                    <select class="chosen-select form-control" name="nama_satuan" id="nama_satuan">
                                                        <option value="">-- Pilih --</option>
                                                        @foreach($unit as $val)
                                                            <option value="{{ $val->id }}">{{ $val->nama_satuan }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Harga Jual</label>
                                                    <input type="text" name="harga_jual" id="harga_jual" class="form-control money" placeholder="Harga jual ..">
                                                </div>
                                                <div class="form-group">
                                                    <label>Deskripsi</label>
                                                    <textarea class="form-control" id="desc" name="desc" rows="4" placeholder="Deskripsi produk ..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Batal</span></button>
                                        <input type="submit" class="btn btn-success" value="Simpan">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- End: modal -->

                    <div id="modalEditData" class="modal" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="blue bigger">Detail Barang</h4>
                                </div>

                                <div class="msgAlert"></div>

                                <form id="updateMasterBarang">
                                    {{ method_field('PUT') }}
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label>SKU</label>
                                                    <input type="text" name="sku_edit" id="sku_edit" class="form-control" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label>Nama Barang</label>
                                                    <input type="hidden" name="id" id="keyEdit"> 
                                                    <input type="text" name="nama_barang_edit" id="nama_barang_edit" class="form-control" placeholder="Nama barang ..">
                                                </div>
                                                <div class="form-group">
                                                    <label>Kategori</label>
                                                    <select class="chosen-select form-control" name="nama_kategori_edit" id="nama_kategori_edit">
                                                        <option value="">-- Pilih Kategori --</option>
                                                        @foreach($kategori as $val)
                                                            <option value="{{ $val->id }}">{{ $val->nama_kategori }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                 <div class="form-group">
                                                    <label>Brand</label>
                                                    <select class="chosen-select form-control" name="nama_brand_edit" id="nama_brand_edit">
                                                        <option value="">-- Pilih Brand --</option>
                                                        @foreach($brand as $val)
                                                            <option value="{{ $val->id }}">{{ $val->nama_brand }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Unit</label>
                                                    <select class="chosen-select form-control" name="nama_satuan_edit" id="nama_satuan_edit">
                                                        <option value="">-- Pilih Unit --</option>
                                                        @foreach($unit as $val)
                                                            <option value="{{ $val->id }}">{{ $val->nama_satuan }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Stok</label>
                                                    <input type="text" name="stok_edit" id="stok_edit" class="form-control" placeholder="Stok barang .." disabled>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Harga Beli</label>
                                                            <input type="text" name="harga_beli_edit" id="harga_beli_edit" class="form-control" placeholder="Harga beli .." style="height: 50px;" disabled>
                                                            <small class="text-info">Harga pembelian terakhir pada barang masuk.</small>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Harga Jual</label>
                                                            <input type="text" name="harga_jual_edit" id="harga_jual_edit" class="form-control money" placeholder="Harga jual .." style="height: 50px; border: 3px solid #5396cd;">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Deskripsi</label>
                                                    <textarea class="form-control" id="desc_edit" name="desc_edit" rows="4" placeholder="Deskripsi produk ..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-success" value="Update">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- End: modal -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<style>
    i.font-icon {
        font-size: 40px;
    }
    #ui-id-1 {
        width: 300px;
        z-index: 9999;
    }
    .ui-menu-item {
        padding: 2px;
        font-size: 14px;
    }
    .ui-menu-item:hover {
        cursor: pointer;
    }
    .alert-error {
        float: left;
    }
</style>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
$(document).on('ready', function() {
    $('#tblData').DataTable().ajax.reload();
});

$('#addStokBarang').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    $.ajax({
        type: "POST",
        url : "{{ '/addStokBarang' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                swal('Success !', data.m, 'success');
                $('#addStokBarang')[0].reset(); // reset form
                $('#tblData').DataTable().ajax.reload();
                $('.modal').modal('hide');
            } else {
                // alert('error !');
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
            }
        },
        error: function(data) {
            let messages = '';
            let msg = data.responseJSON; // An array with all errors.
            $.each(msg.errors, function(key, value) {
                messages += '<span class="alert-error">'+value+'</span>';
            });
            toastr.error(messages);
        }
    }); 
});

$('#addMasterBarang').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    var nama_barang = $("#nama_barang").val();

    $.ajax({
        type: "POST",
        url : "{{ '/saveBarang' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                swal('Success !', data.m, 'success');
                $('#tblData').DataTable().ajax.reload();
                $('.modal').modal('hide');
                $('#addMasterBarang')[0].reset(); // reset form
            } else {
                // alert('error !');
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
            }
        },
        error: function(data) {
            let messages = '';
            let msg = data.responseJSON; // An array with all errors.
            $.each(msg.errors, function(key, value) {
                messages += '<span class="alert-error">'+value+'</span>';
            });
            toastr.error(messages);
        }
    }); 
});

$(function() {
    $('#tblData').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/getDataBarang',
        columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
                    { data: 'sku', name: 'sku' },
                    { data: 'nama_barang', name: 'nama_barang' },
                    { data: 'nama_kategori', name: 'nama_kategori' },
                    { data: 'stok', name: 'stok' },
                    { data: 'harga_jual', name: 'harga_jual' },
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                 ],
                "order": [[ '1', "desc" ]]
    });
 });

function editBarang(param) {
    $.ajax({
        type: "GET",
        url : "{{ '/barang/edit/' }}"+param,
        dataType: "json",
        success: function(data) {
            if (data.s == 'success') {
                $('#modalEditData').modal();
                $('#keyEdit').val(param);
                $('#sku_edit').val(data.data[0].sku);
                $('#nama_barang_edit').val(data.data[0].nama_barang);
                $('#nama_kategori_edit').val(data.data[0].id_kategori);
                $('#nama_brand_edit').val(data.data[0].id_brand);
                $('#nama_satuan_edit').val(data.data[0].id_unit);
                $('#stok_edit').val(data.data[0].stok);
                $('#harga_jual_edit').val(formatCurrency(data.data[0].harga_jual));
                if (data.lastPriceStockIn != 0) {
                    $('#harga_beli_edit').val(formatCurrency(data.lastPriceStockIn));
                } else {
                    $('#harga_beli_edit').val('0');
                }
                $('#desc_edit').val(data.data[0].desc);
            } else {
                alert('error');
            }
        }
    }); 
}

$('#updateMasterBarang').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    var nama_barang_edit = $("#nama_barang_edit").val();

    if (nama_barang_edit == '') {
        $('.msgAlert').show().html('<div class="alert alert-danger">Nama barang tidak boleh kosong !</div');
    } else {
        $.ajax({
            type: "POST",
            url : "{{ '/updateBarang' }}",
            data: formData,
            dataType: "json",
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data.s == 'success') {
                    $('.modal').modal('hide');
                    swal('Success !', data.m, 'success');
                    $('#tblData').DataTable().ajax.reload();
                } else {
                    $('.msgAlert').show().html('<div class="alert alert-danger">'+data.m+'</div');
                }
            }
        }); 
    }
});

function deleteBarang(param) {
    if (confirm('Are you sure you want to delete this?')) {
        $.ajax({
            type: "GET",
            url : "{{ '/barang/delete/' }}"+param,
            dataType: "json",
            success: function(data) {
                if (data.s == 'success') {
                    $('#nama_barang_edit').val(data.m);
                    swal('Success !', data.m, 'success');
                    $('#tblData').DataTable().ajax.reload();
                } else {
                    alert('error');
                }
            }
        }); 
    }
}
</script>

@endsection