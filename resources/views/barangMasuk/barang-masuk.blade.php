@extends('master')

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="{{ url('./') }}">Home</a>
                </li>
                <li class="active">Barang Masuk</li>
            </ul><!-- /.breadcrumb -->
        </div>
        
        <!-- ISI CONTENT -->
        <div class="page-content">
            
            @include('includes.setting-template')

            <div class="page-header">
                <h1>
                    Barang Masuk
                </h1>
            </div><!-- /.page-header -->
            <br>

            <div class="row">
                <div class="col-xs-12">
                    <a href="#modalAddData" role="button" class="btn btn-danger btn-sm btn-add" data-toggle="modal" id="addData"> Add Data </a>
                    <br><br>
                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table class="table table-hover" id="tblData">
                               <thead>
                                    <tr>
                                        <th width="20px">No</th>
                                        <th>No Faktur</th>
                                        <th>Tanggal</th> 
                                        <th>Supplier</th>
                                        <th>Total</th>
                                        <th>Note</th>
                                        <th width="80px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="modalAddData" class="modal" tabindex="-1" data-keyboard="false" data-backdrop="static">
                        <div class="modal-dialog modalXLarge">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="blue bigger">Tambah Barang Masuk</h4>
                                </div>

                                <div class="msgAlert"></div>

                                <form id="addBarangMasuk">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="total_harga" id="total_harga">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label>Tanggal Masuk</label>
                                                        <input class="form-control datepicker" name="date_in" id="date_in" type="text" autocomplete="off">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>No Faktur</label>
                                                        <input type="text" name="no_faktur" id="no_faktur" class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label>Supplier</label>
                                                        <select class="chosen-select form-control" name="supplier" id="supplier">
                                                            <option value="">-- Pilih Supplier --</option>
                                                            @foreach($supplier as $val)
                                                                <option value="{{ $val->nama_supplier }}">{{ $val->nama_supplier }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>  
                                            </div>
                                            
                                            <div class="col-lg-6">
                                                <div class="tile">
                                                    <div class="form-group">
                                                        <label>Note</label>
                                                        <textarea class="form-control" rows="5" name="note" placeholder="Catatan jika ada ..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr style="border: 2px dotted #eff3f8;">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12">
                                                <div class="form-group">
                                                    <label>Cari Barang</label>
                                                    <input type="text" name="productSelect" id="productSelect" class="form-control" placeholder="Search by product name or sku .." style="border: 15px solid #eff3f8; height: 80px; padding: 5px 15px;">
                                                    </div>  
                                            </div>
                                        </div>
                                        <hr style="border: 2px dotted #eff3f8;">
                                        <div class="row">
                                            <div class="col-lg-12 mt-30">
                                                <div class="tile table-responsive" style="position: relative;">
                                                    <label class="col-form-label col-form-label-sm label-calculation">List Barang Masuk</label>
                                                    <table id="tableCart" class="table" style="border: 1px solid #ccc; background: #fff;">
                                                        <thead style="line-height: 30px; font-size: 12px;">
                                                            <tr>
                                                                <th width="10px">No</th>
                                                                <th width="130px">SKU</th>
                                                                <th>NAMA BARANG</th>
                                                                <th width="120px">STOK GUDANG</th>
                                                                <th width="120px">STOK MASUK</th>
                                                                <th width="180px">HARGA BELI (Rp)</th>
                                                                <th width="180px">SUBTOTAL (Rp)</th>
                                                                <th width="50px" style="text-align: right;">#</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="bodyListData">
                                                            <img src="images/loading.gif" id="loading" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="6" class="total">TOTAL (Rp.)</td>
                                                                <td class="total total-numb text-right" id="totalWhenCreateData"></td>
                                                                <td class="total">   
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Batal</span></button>
                                        <input type="submit" class="btn btn-success" value="Simpan">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- End: modal -->

                    <div id="modalDetailData" class="modal" tabindex="-1">
                        <div class="modal-dialog modalXLarge">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="blue bigger">Detail Barang Masuk</h4>
                                </div>

                                <div class="msgAlert"></div>

                                <form id="MasukMasuk">
                                    {{ method_field('PUT') }}
                                    <input type="hidden" name="id" id="keyEdit"> 
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label>Tanggal Masuk</label>
                                                        <input class="form-control" name="date_in_edit" id="date_in_edit" type="text" autocomplete="off" disabled>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>No Faktur</label>
                                                        <input type="text" name="no_faktur_edit" id="no_faktur_edit" class="form-control" disabled>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label>Supplier</label>
                                                        <select class="chosen-select form-control" name="supplier_edit" id="supplier_edit" disabled>
                                                            <option value="">-- Pilih Supplier --</option>
                                                            @foreach($supplier as $val)
                                                                <option value="{{ $val->nama_supplier }}">{{ $val->nama_supplier }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>  
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="tile">
                                                    <div class="form-group">
                                                        <label>Note</label>
                                                        <textarea class="form-control" rows="5" name="note_edit" id="note_edit" placeholder="Catatan jika ada ..." disabled></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr style="border: 2px dotted #eff3f8;">
                                        <div class="row">
                                            <div class="col-lg-12 mt-30">
                                                <div class="tile table-responsive" style="position: relative;">
                                                    <label class="col-form-label col-form-label-sm label-calculation">List Barang Masuk</label>
                                                    <table id="tableCartetail" class="table" style="border: 1px solid #ccc; background: #fff;">
                                                        <thead style="line-height: 30px; font-size: 12px;"">
                                                            <tr>
                                                                <th width="10px">No</th>
                                                                <th width="130px">SKU</th>
                                                                <th>NAMA BARANG</th>
                                                                <th width="120px">STOK MASUK</th>
                                                                <th width="180px">HARGA BELI (Rp)</th>
                                                                <th width="180px">SUBTOTAL (Rp)</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="bodyListDataDetail">
                                                            <img src="images/loading.gif" id="loading" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="5" class="total">TOTAL (Rp.)</td>
                                                                <td class="total total-numb text-right" id="totalWhenCreateData_edit"></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="modal-footer">
                                        <input type="submit" class="btn btn-success" value="Simpan">
                                    </div> -->
                                </form>
                            </div>
                        </div>
                    </div><!-- End: modal -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<style>
    .alert-error {
        float: left;
    }
</style>

<script>
var count = 0;
$(document).on('ready', function() {
    $('#tblData').DataTable().ajax.reload();
});

// hide info alert
$('.msgAlert').delay(5000).fadeOut(1000);

$(document).on('click', '.btn-add, .btn-edit', function() {
    $('.msgAlert').hide();
});

$('#addBarangMasuk').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    var nama_barang = $("#barang").val();

    $.ajax({
        type: "POST",
        url : "{{ '/saveBarangMasuk' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                swal('Success !', data.m, 'success');
                $('#tblData').DataTable().ajax.reload();
                $('.modal').modal('hide');
                $('#addBarangMasuk')[0].reset(); // reset form
            } 
            else if (data.s == 'fail') {
                swal('Error !', data.m, 'error');
            }
            else {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
            }
        },
        error: function(data) {
            let messages = '';
            let msg = data.responseJSON; // An array with all errors.
            $.each(msg.errors, function(key, value) {
                messages += '<span class="alert-error">'+value+'</span>';
            });
            toastr.error(messages);
        }
    }); 
});

$(function() {
    $('#tblData').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/getDataBarangMasuk',
        columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
            { data: 'no_faktur', name: 'no_faktur' },
            { data: 'tanggal', name: 'tanggal' },
            { data: 'supplier', name: 'supplier' },
            { data: 'total_harga', name: 'total_harga' },
            { data: 'note', name: 'note' },
            { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        // "order": [[ '1', "desc" ]]
    });
 });

 function detailBarangMasuk(param) {
    $.ajax({
        type: "GET",
        url : "{{ '/barangMasuk/edit/' }}"+param,
        dataType: "json",
        success: function(data) {
            if (data.s == 'success') {
                $('#modalDetailData').modal();
                $('#keyID').val(data.respon.header.idMasuk);
                $('#no_faktur_edit').val(data.respon.header.no_faktur);
                $('#date_in_edit').val(data.respon.header.tanggal);
                $('#supplier_edit').val(data.respon.header.supplier);
                $('#note_edit').val(data.respon.header.note);
                // -----------------------------------------------
                var html = '';
                var no = 1;
                var FOTMATSUBTOTAL = '';
                var FOTMATPRICEITEM = '';
                for (i=0; i<data.respon.detail.length; i++) {
                    FOTMATPRICEITEM = formatCurrency(data.respon.detail[i].harga);
                    subtotal = parseInt(data.respon.detail[i].jumlah) * parseInt(data.respon.detail[i].harga)
                    FOTMATSUBTOTAL = formatCurrency(subtotal);
                    html += `<tr id="row-${data.respon.detail[i].id}">
                            <td class='content-tabe sn'>${no++}</td> 
                            <td class='content-tabe sku'>${data.respon.detail[i].sku}</td>
                            <td class='content-tabe content-product_name'>${data.respon.detail[i].nama_barang}</td>
                            <td align="center" class="qty ${data.respon.detail[i].id} content-tabe content-qty">
                                ${data.respon.detail[i].jumlah}
                            </td> 
                            <td align="right" class="content-tabe content-selling_price">${FOTMATPRICEITEM}</td> 
                            <td align="right" id="subTot-${data.respon.detail[i].id}" class="content-tabe content-subTot">
                                ${FOTMATSUBTOTAL}
                                <input type="hidden" class="mySubTotInLoop" value="${data.respon.detail[i].subtotal}">
                            </td>     
                        </tr>
                        `;
                }
                $('#bodyListDataDetail').html(html);
                // ---------------------------------------
                var TOTAL = formatCurrency(data.respon.header.total_harga);
                $('#totalWhenCreateData_edit').text(TOTAL);
            } 
            else {
                alert('error');
            }
        }
    }); 
}

$('#addData').on('click', function() {
    $('#addBarangMasuk')[0].reset();
    $('#totalWhenCreateData').empty();
    $('#total_harga').val('');
    $("tbody#bodyListData").each(function() {
        var last = $(this).find('tr').last().attr('id');
        if ($("tbody#bodyListData tr").length) {
            var getID = last.replace("row-", "");
            for (i=1; i<=getID; i++) {
                $('tr#row-'+i).remove();
            }
        }
    });
});

$(function () {
    // search item
    $('#productSelect').autocomplete({
        source: function( request, response ) {
            // Fetch data
            $.ajax({
                url:"{{ '/search/product' }}",
                type: 'post',
                dataType: "json",
                data: {
                   _token: '{{ csrf_token() }}',
                   search: request.term
                },
                beforeSend: function(data) {
                    $('body').css('overflow-y', 'hidden');
                },
                success: function(data) {
                    if (data.data == 0) {
                        toastr.error(data.m);
                        $('.ui-autocomplete').css('display', 'none');
                    } else {
                        response(data);
                    }
                }
            });
        },
        select: function (event, ui) {
            $('#productSelect').prop('readonly', true);
            var purchasePrice = ui.item.harga_beli != null ? ui.item.harga_beli : 0;
            callItemList(ui.item.sku, ui.item.item, 1, purchasePrice, ui.item.harga_jual);
            $('#modalAddData').animate({
                scrollTop: $("#tableCart").offset().top
            }, 1500);
        }   
    });
});

function callItemList(sku, item, qty, purchasePrice, sellingPrice) {
    $('#loading').show();
    setTimeout(function() { 
        $('#loading').hide();
        addItem(); // add item to cart
        // -------------------------------------
        setTimeout(function() { 
            // searchLastElement(); 
            $("tbody#bodyListData").each(function() {
                var last = $(this).find('tr').last().attr('id');
                var getID = last.replace("row-", "");
                // -----------------------------------
                $.ajax({
                    type: "GET",
                    url : "{{ '/checkStockInWarehouse/' }}"+sku,
                    dataType: "json",
                    async: false,
                    success: function(data) {
                        if (data.s == 'success') {
                            $('#infoStockGudang-'+getID).val(data.lastStock);
                        } 
                    }
                }); 
                // ------------------------------------
                $('#mySku-'+getID).val(sku);
                $('#myItem-'+getID).val(item);
                $('#myQty-'+getID).val(qty);
                $('#myPurchasePrice-'+getID).val(purchasePrice);
                $('#purchasePrice-'+getID).val(formatCurrency(purchasePrice));
                $('#subtotal-'+getID).val(formatCurrency(purchasePrice * qty));
                $('#subtotalHidden-'+getID).val(purchasePrice * qty);
                var subTot = 0;
                $("#bodyListData tr").each(function() {
                    subTot += parseInt($(this).find(".mySubtotal").val());
                });
                console.log(subTot)
                $('#totalWhenCreateData').text(formatCurrency(subTot));
                $('#total_harga').val(subTot);
            });
        }, 100);
        return false;
    }, 500);
}

function addItem() {
    $('#productSelect').prop('readonly', false).val('');
	var no = count + 1;
	count++;
    // alert(no)
    var html = `
        <tr id="row-${no}">
            <td class="noListData">
                ${no}
            </td>
            <td>
                <input type="text" name="mySku[]" id="mySku-${no}" class="form-control" readonly>
            </td>
            <td>
                <input type="text" name="myItem[]" id="myItem-${no}" class="form-control" readonly>
            </td>
            <td>
                <input type="text" name="infoStockGudang[]" id="infoStockGudang-${no}" class="form-control" readonly>
            </td>
            <td>
                <input type="text" name="myQty[]" id="myQty-${no}" class="form-control" onkeyup="changeQty(this.id, this.value)">
            </td>
            <td>
                <input type="text" name="purchasePrice[]" id="purchasePrice-${no}" class="form-control money text-right" onkeyup="changePurchasePrice(this.id, this.value)" autocomplete="off" style="width: 160px;">
                <input type="hidden" name="myPurchasePrice[]" id="myPurchasePrice-${no}">
            </td>`;
        html += `
            <td>
                <input type="text" id="subtotal-${no}" class="form-control money text-right" style="width: 160px;" readonly>
                <input type="hidden" id="subtotalHidden-${no}" class="form-control mySubtotal" value="0">
            </td>
            <td>
                <div class="hidden-sm hidden-md btn-group"> 
                    <button type="button" class="btn btn-md btn-danger btn-remove" title="Remove" id="removeId-${no}" data-id="remove-${no}" onclick="removeItem(this.id)">
                        <i class="fa fa-times" aria-hidden="true" style="line-height: 23px;"></i>
                    </button>
                </div>
            </td>
        </tr>`;
	$("#bodyListData").append(html).hide().show('slow');;
    noAutoincrement();
}

function noAutoincrement() {
    $("td.noListData").each(function(i, v) {
        $(v).text(i + 1);
    });
}

$(document).on( 'click', '.delete-record', function(e) {
	e.preventDefault();    
	var key = $(this).attr('data-id');
    var getID = key.replace("remove-", "");
    $('#row-'+ getID).remove();
    // -----------------------------------
    noAutoincrement();
    return true;
});

function removeItem(id) {  
	var getID = id.replace("removeId-", "");
    $('#row-'+ getID).remove();
    // -----------------------------------
    noAutoincrement();

    setTimeout(function() {   
        // Hitung ulang total
        var subTot = 0;
        $("#bodyListData tr").each(function() {
            subTot += parseInt($(this).find(".mySubtotal").val());
        });
        $('#totalWhenCreateData').text(formatCurrency(subTot));
        $('#total_harga').val(subTot);
    }, 500);
}

function changePurchasePrice(id, val) {
    var getID = id.replace("purchasePrice-", "");
    var getValue = val.replace(/\./g, '');
    var qty = $('#myQty-'+getID).val();
    var subtotal = parseInt(getValue) * parseInt(qty);
    $('#myPurchasePrice-'+getID).val(getValue);
    $('#subtotal-'+getID).val(formatCurrency(subtotal));
    $('#subtotalHidden-'+getID).val(subtotal);
    // Hitung ulang total
    var subTot = 0;
    $("#bodyListData tr").each(function() {
        subTot += parseInt($(this).find(".mySubtotal").val());
    });
    $('#totalWhenCreateData').text(formatCurrency(subTot));
    $('#total_harga').val(subTot);
}

function changeQty(id, val) {
    var getID = id.replace("myQty-", "");
    var getValue = val.replace(/\./g, '');
    var myPurchasePrice = $('#myPurchasePrice-'+getID).val();
    var subtotal = parseInt(getValue) * parseInt(myPurchasePrice);
    $('#subtotal-'+getID).val(formatCurrency(subtotal));
    $('#subtotalHidden-'+getID).val(subtotal);
    // Hitung ulang total
    var subTot = 0;
    $("#bodyListData tr").each(function() {
        subTot += parseInt($(this).find(".mySubtotal").val());
    });
    $('#totalWhenCreateData').text(formatCurrency(subTot));
    $('#total_harga').val(subTot);
}

$('#MasukMasuk').on('submit', function(e) {
    e.preventDefault();
    var formData = new FormData(this);
    var nama_barang_edit = $("#nama_barang_edit").val();

    if (nama_barang_edit == '') {
        $('.msgAlert').show().html('<div class="alert alert-danger">Nama barang tidak boleh kosong !</div');
    } else {
        $.ajax({
            type: "POST",
            url : "{{ '/updateBarangMasuk' }}",
            data: formData,
            dataType: "json",
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data.s == 'success') {
                    swal('Success !', data.m, 'success');
                    $('#tblData').DataTable().ajax.reload();
                } else {
                    $('.msgAlert').show().html('<div class="alert alert-danger">'+data.m+'</div');
                }
            },
            error: function(data) {
                var errors = data.responseJSON; // An array with all errors.
                $.each(errors.errors, function(key, value) {
                    $('.msgAlert').show().append('<div class="alert alert-danger">'+value+'</div');
                });
            }
        }); 
    }
});

function deleteBarangMasuk(param) {
    if (confirm('Are you sure you want to delete this?')) {
        $.ajax({
            type: "GET",
            url : "{{ '/barangMasuk/delete/' }}"+param,
            dataType: "json",
            success: function(data) {
                if (data.s == 'success') {
                    $('#nama_barang_edit').val(data.m);
                    swal('Success !', data.m, 'success');
                    $('#tblData').DataTable().ajax.reload();
                } else {
                    alert('error');
                }
            }
        }); 
    }
}
</script>

@endsection