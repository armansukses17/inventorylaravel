			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">Apps</span>
							Inventory New &copy; {{ date('Y') }}
						</span>

						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
							</a>
						</span>
					</div>
				</div>
			</div>


			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('/assets/js/ace-extra.min.js') }}"></script>
		<script src="{{ asset('/assets/js/ace-elements.min.js') }}"></script>
		<script src="{{ asset('/assets/js/ace.min.js') }}"></script>
		<script src="{{ asset('/assets/js/myDatatables/jquery.dataTables.min.js') }}"></script>
		<script src="{{ asset('/assets/js/sweetalert.min.js') }}"></script>
		<script src="{{ asset('/assets/js/toastr.min.js') }}"></script>
		<script src="{{ asset('/assets/js/bootstrap-datepicker.min.js') }}"></script>
		<script src="{{ '/js/jquery.mask.js' }}"></script>
		<script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            orientation: "bottom auto",
            autoclose: true
        });
        // Bootstrap Date picker in modal
        $("body").delegate(".datepicker-bts-modal, .datepicker-bts-extreme", "focusin", function () {
            $(this).datepicker({
                format: 'yyyy-mm-dd',
                orientation: "bottom auto",
                autoclose: true
            });
        });
        // Only Number
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        // format uang
        function formatCurrency(num) {
            var	number_string = num.toString(),
                sisa 	= number_string.length % 3,
                rupiah 	= number_string.substr(0, sisa),
                ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
            // ---------------------------------
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            return rupiah;
        }
        // Format mata uang.
        $('.money').mask('#.##0', {reverse: true});
        // Format telp
        $('.phoneNumb').mask('0000-0000-00000');
        // time
        $('.time').mask('00:00');
    </script>
	</body>
</html>
