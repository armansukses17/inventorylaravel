<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php //echo $title; ?></title>

		<meta name="description" content="Static &amp; Dynamic Tables" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{ asset('/assets/css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/assets/font-awesome/4.5.0/css/font-awesome.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/assets/css/fonts.googleapis.com.css') }}" />
		
		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="{{ asset('/assets/css/jquery-ui.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/assets/css/jquery-ui.custom.min.css') }}" />

		<link rel="stylesheet" href="{{ asset('/assets/css/bootstrap-datepicker3.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/assets/css/bootstrap-timepicker.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/assets/css/daterangepicker.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/assets/css/bootstrap-datetimepicker.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/assets/css/ace.min.css') }}" class="ace-main-stylesheet" id="main-ace-style" />
		<link rel="stylesheet" href="{{ asset('/assets/css/ace-skins.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/assets/css/ace-rtl.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/assets/css/myDatatables/jquery.dataTables.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('/assets/css/toastr.min.css') }}" />
		<link rel="stylesheet" href="{{ '/css/custom.css' }}" />
		<style>
			.fade-scale {
			  transform: scale(0);
			  opacity: 0;
			  -webkit-transition: all .25s linear;
			  -o-transition: all .25s linear;
			  transition: all .25s linear;
			}

			.fade-scale.in {
			  opacity: 1;
			  transform: scale(1);
			}
		</style>	
		
		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="{{ asset('/assets/css/ace-ie.min.css') }}" />
		<![endif]-->

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
		<!--[if lte IE 8]>
		<script src="{{ asset('/assets/js/html5shiv.min.js') }}"></script>
		<script src="{{ asset('/assets/js/respond.min.js') }}"></script>
		<![endif]-->

		<!-- ----------------------------------------------------------------------------------- -->

		<!--[if !IE]> -->
		<script src="{{ asset('/assets/js/jquery-2.1.4.min.js') }}"></script>
		<script src="{{ asset('/assets/js/jquery-ui.min.js') }}"></script>
		
		<!-- <![endif]-->

		<!--[if IE]>
		<script src="{{ asset('/assets/js/jquery-1.11.3.min.js') }}"></script>
		<![endif]-->
	</head>

	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="{{ './' }}" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i>
							Inventory
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="purple dropdown-modal">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-bell icon-animated-bell"></i>
								<span class="badge badge-important">8</span>
							</a>

							<ul class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="ace-icon fa fa-exclamation-triangle"></i>
									8 Notifications
								</li>

								<li class="dropdown-content">
									<ul class="dropdown-menu dropdown-navbar navbar-pink">
										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">
														<i class="btn btn-xs no-hover btn-pink fa fa-comment"></i>
														New Comments
													</span>
													<span class="pull-right badge badge-info">+12</span>
												</div>
											</a>
										</li>

										<li>
											<a href="#">
												<i class="btn btn-xs btn-primary fa fa-user"></i>
												Bob just signed up as an editor ...
											</a>
										</li>

										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">
														<i class="btn btn-xs no-hover btn-success fa fa-shopping-cart"></i>
														New Orders
													</span>
													<span class="pull-right badge badge-success">+8</span>
												</div>
											</a>
										</li>

										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">
														<i class="btn btn-xs no-hover btn-info fa fa-twitter"></i>
														Followers
													</span>
													<span class="pull-right badge badge-info">+11</span>
												</div>
											</a>
										</li>
									</ul>
								</li>

								<li class="dropdown-footer">
									<a href="#">
										See all notifications
										<i class="ace-icon fa fa-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						<li class="light-blue dropdown-modal">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="{{ asset('assets/files/users/'.auth()->user()->photo) }}" alt="Jason's Photo" />
								<span class="user-info">
									<small>Welcome,</small>
									{{ auth()->user()->fullName }}
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="{{ '/masterUser/detail/'. auth()->user()->id }}">
										<i class="ace-icon fa fa-user"></i>
										Profile
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="{{ url('/logout') }}">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>