<div id="sidebar" class="sidebar responsive ace-save-state">
	<script type="text/javascript">
		try{ace.settings.loadState('sidebar')}catch(e){}
	</script>

	<div class="sidebar-shortcuts" id="sidebar-shortcuts">
		<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
			<button class="btn btn-success">
				<i class="ace-icon fa fa-signal"></i>
			</button>

			<button class="btn btn-info">
				<i class="ace-icon fa fa-pencil"></i>
			</button>

			<button class="btn btn-warning">
				<i class="ace-icon fa fa-users"></i>
			</button>

			<button class="btn btn-danger">
				<i class="ace-icon fa fa-cogs"></i>
			</button>
		</div>

		<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
			<span class="btn btn-success"></span>

			<span class="btn btn-info"></span>

			<span class="btn btn-warning"></span>

			<span class="btn btn-danger"></span>
		</div>
	</div><!-- /.sidebar-shortcuts -->

	<ul class="nav nav-list">
		<li class="<?php if (request()->segment(1) == '') { echo 'active'; } ?>">
			<a href="{{ url('./') }}">
				<i class="menu-icon fa fa-tachometer"></i>
				<span class="menu-text"> Dashboard </span>
			</a>

			<b class="arrow"></b>
		</li>

		<!-- jika admin -->
		@if (auth()->user()->level == '2')
			<?php 
			if (request()->segment(1) == 'masterKategori' ||
				request()->segment(1) == 'masterBrand' ||
				request()->segment(1) == 'masterUnit' ||
				request()->segment(1) == 'masterSupplier' ||
				request()->segment(1) == 'masterCustomer'
			) { 
				$open = 'open'; 
			} else {
				$open = ''; 
			}
			?>
			<li class="{{ $open }}">
				<a href="#" class="dropdown-toggle">
					<i class="menu-icon fa fa-list"></i>
					<span class="menu-text"> Data Master </span>
					<b class="arrow fa fa-angle-down"></b>
				</a>

				<b class="arrow"></b>

				<ul class="submenu">	
					<li class="<?php if (request()->segment(1) == 'masterKategori') { echo 'active'; } ?>">
						<a href="{{ url('/masterKategori') }}">
							<i class="menu-icon fa fa-caret-right"></i>
							Kategori
						</a>
						<b class="arrow"></b>
					</li>
					<li class="<?php if (request()->segment(1) == 'masterBrand') { echo 'active'; } ?>">
						<a href="{{ url('/masterBrand') }}">
							<i class="menu-icon fa fa-caret-right"></i>
							Brand
						</a>
						<b class="arrow"></b>
					</li>
					<li class="<?php if (request()->segment(1) == 'masterUnit') { echo 'active'; } ?>">
						<a href="{{ url('/masterUnit') }}">
							<i class="menu-icon fa fa-caret-right"></i>
							Satuan
						</a>
						<b class="arrow"></b>
					</li>
					<li class="<?php if (request()->segment(1) == 'masterSupplier') { echo 'active'; } ?>">
						<a href="{{ url('/masterSupplier') }}">
							<i class="menu-icon fa fa-caret-right"></i>
							Supplier
						</a>
						<b class="arrow"></b>
					</li>
					<li class="<?php if (request()->segment(1) == 'masterCustomer') { echo 'active'; } ?>">
						<a href="{{ url('/masterCustomer') }}">
							<i class="menu-icon fa fa-caret-right"></i>
							Customer
						</a>
						<b class="arrow"></b>
					</li>
				</ul>
			</li>

			<li class="<?php if (request()->segment(1) == 'dataBarang') { echo 'active'; } ?>">
				<a href="{{ url('/dataBarang') }}">
					<i class="menu-icon fa fa-list-alt"></i>
					<span class="menu-text"> Master Barang </span>
				</a>
				<b class="arrow"></b>
			</li>

			<?php 
			if (request()->segment(1) == 'barangMasuk' ||
				request()->segment(1) == 'barangKeluar'
			) { 
				$open = 'open'; 
			} else {
				$open = ''; 
			}
			?>
			<li class="{{ $open }}">
				<a href="#" class="dropdown-toggle">
					<i class="menu-icon fa fa-list"></i>
					<span class="menu-text"> Transaksi </span>
					<b class="arrow fa fa-angle-down"></b>
				</a>
				<b class="arrow"></b>

				<ul class="submenu">
					<li class="<?php if (request()->segment(1) == 'barangMasuk') { echo 'active'; } ?>">
						<a href="{{ url('/barangMasuk') }}">
							<i class="menu-icon fa fa-caret-right"></i>
							Barang Masuk
						</a>
						<b class="arrow"></b>
					</li>

					<li class="<?php if (request()->segment(1) == 'barangKeluar') { echo 'active'; } ?>">
						<a href="{{ url('/barangKeluar') }}">
							<i class="menu-icon fa fa-caret-right"></i>
							Barang Keluar
						</a>
						<b class="arrow"></b>
					</li>
				</ul>
			</li>
		@endif

		<li>
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-list"></i>
				<span class="menu-text"> Laporan </span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="<?php if (request()->segment(1) == 'lapStokBarang') { echo 'active'; } ?>">
					<a href="{{ url('/lapStokBarang') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						Stok Barang
					</a>
					<b class="arrow"></b>
				</li>
			</ul>
		</li>

		<?php 
		if (request()->segment(1) == 'masterUser') { 
			$open = 'open'; 
		} else {
			$open = ''; 
		}
		?>

		<li class="{{ $open }}">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-list"></i>
				<span class="menu-text"> Pengaturan </span>
				<b class="arrow fa fa-angle-down"></b>
			</a>

			<b class="arrow"></b>

			<ul class="submenu">
				<li class="<?php if (request()->segment(1) == 'masterUser') { echo 'active'; } ?>">
					<a href="{{ url('/masterUser') }}">
						<i class="menu-icon fa fa-caret-right"></i>
						Data User
					</a>
					<b class="arrow"></b>
				</li>
			</ul>
		</li>

		<li class="">
			<a href="{{ url('/logout') }}">
				<i class="menu-icon fa fa-list-alt"></i>
				<span class="menu-text"> logout </span>
			</a>
			<b class="arrow"></b>
		</li>
	</ul><!-- /.nav-list -->
	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
</div>
