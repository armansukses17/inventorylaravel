@extends('master')

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="{{ url('./') }}">Home</a>
                </li>
                <li class="active">Laporan Stok Barang</li>
            </ul><!-- /.breadcrumb -->
        </div>
        
        <!-- ISI CONTENT -->
        <div class="page-content">
            
            @include('includes.setting-template')

            <div class="page-header">
                <h1>
                   Laporan Stok Barang
                </h1>
            </div><!-- /.page-header -->
            <br>

            <div class="row">
                <div class="col-xs-12">

                   <!-- <form id="formFilter">
                        <div class="form-actions">
                            <div class="input-group" style="position: relative;">
                                <span style="position: absolute;left: 10px;top: -6px;z-index: 4; padding: 0 5px;background: #6fb3e0;font-size: 8px;color: #fff;">Tanggal Keluar</span>
                                <input type="text" class="form-control datepicker" name="tglKeluar" id="tglKeluar" autocomplete="off" width="200px">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-sm btn-primary"><i class="ace-icon fa fa-share"></i> Filter</button>
                                    <button class="btn btn-sm btn-danger no-radius" type="button" id="btnDownload" disabled="disabled">
                                        <i class="ace-icon fa fa-download"></i>
                                        Download
                                    </button>
                                </span>
                            </div>
                        </div>
                    </form> -->

                    <div class="tile">
                        <div class="row">
                            <div class="col-md-12 mb-3"><strong>Filter Berdasarkan :</strong></div>
                        </div>
                        <form class="row" id="formFilter3">
                            <div class="form-group col-md-2">
                                <select class="chosen-select form-control" name="filterType3" id="filterType3">
                                    <option value="">-- Pilih --</option>
                                    <option value="1">Bulan</option>
                                    <option value="2">Tahun</option>
                                </select>
                            </div>
                            <div class="form-group col-md-5" id="wrapperstartFilter3" style="position: relative;">
                                <input type="text" name="sementara3" id="sementara3" class="form-control" disabled>
                                <img src="{{ '/images/loading.gif' }}" id="loading3" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">
                            </div>
                            <div class="form-group col-md-5">
                                <button type="submit" class="btn btn-primary btn-sm" id="btnFilter3" disabled><i class="fa fa-search"></i> Load</button>
                                <span id="wrapDownload3"></span>
                            </div>
                        </form>
                    </div>

                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table class="table table-hover" id="tblData">
                               <thead>
                                    <tr>
                                        <th width="20px">No</th>
                                        <th>Sku</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah Masuk</th>
                                        <th>Jumlah Keluar</th>
                                        <!-- <th>Stok Akhir</th> -->
                                    </tr>
                                </thead>
                                <tbody id="bodyListData">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->


<script>
$(document).on('ready', function() {
});

$(function() {
    // var myData = $("#tblData").DataTable({
    //     processing: true,
    //     serverSide: true,
    //     ajax: {
    //         "url"  : "/getLapStok",
    //         "data" : function (d) {
    //             d.param1 = $('#tglKeluar').val();
    //             d.param2 = $('#tglKeluar').val();
    //         }
    //     },
    //     columns: [
    //                 { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
    //                 { data: 'sku', name: 'sku' },
    //                 { data: 'nama_barang', name: 'nama_barang' },
    //                 { data: 'jml_masuk', name: 'jml_masuk' },
    //                 { data: 'jml_keluar', name: 'jml_keluar' },
    //                 // { data: 'stokAkhir', name: 'stokAkhir' }
    //              ],
    //             "order": [[ '1', "desc" ]]
    // });

    // $('#formFilter').on('submit',function(e) {
    //     e.preventDefault();
    //     var tglKeluar = $('#tglKeluar').val();
    //     if (tglKeluar == '') {
    //         swal('Error !', 'Field anggal tidak boleh kosong', 'error');
    //     } else {
    //         $('#btnDownload').prop('disabled', false);
    //         // myData.ajax.reload();
    //         myData.draw();
    //     }
    // });
});

// ############################################################
function callingData() {
    listData();
}

function nonActiveBtnFilter3() {
    $('#wrapDownload3').html(`<button class="btn btn-success btn-sm" disabled> Download Excels </button>`);
}

function changeYear3() {
    nonActiveBtnFilter3();
}

function changeMonth3() {
    nonActiveBtnFilter3();
}

function listData(filterType3='', filterBy3='', yearWithMonth3='') {
    $.ajax({
        type: "GET",
        url : "{{ '/getLapStok' }}",
        data: {
            //  _token: '{{ csrf_token() }}', 
            filterType3: filterType3, 
            filterBy3: filterBy3,
            yearWithMonth3: yearWithMonth3
        },
        dataType: "json",
        async: false, 
        success: function(data) {
            if (data.s == 'success') {
                var html = '';
                var start = '';
                var jml_masuk = '';
                var jml_keluar = '';
                for (i=0; i<data.m.length; i++) {
                    start = i + 1;
                    jml_masuk = (data.m[i].jml_masuk != null) ? data.m[i].jml_masuk : '-';
                    jml_keluar = (data.m[i].jml_keluar != null) ? data.m[i].jml_keluar : '-';
                    html += `<tr id="row-${start}">
                            <td>${start}</td>
                            <td>${data.m[i].sku}</td>
                            <td>${data.m[i].nama_barang}</td>
                            <td>${jml_masuk}</td>
                            <td>${jml_keluar}</td>
                        </tr>
                    `;
                }
                $('#bodyListData').html(html);
            } 
            else {
                alert('error');
            }
        }
    }); 
}

$('#filterType3').change(function() {
    $('#bodyListData').empty();
    nonActiveBtnFilter3();
    var type = $(this).val();
    $('#loading3').show();
    var loadingHide = `<img src="{{ '/images/loading.gif' }}" id="loading3" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">`;
    var loadingHide2 = ` <input type="text" name="sementara3" id="sementara3" class="form-control" disabled>
                        <img src="{{ '/images/loading.gif' }}" id="loading3" alt="Loading ..." style="display: none; position: absolute; top: 50%; left: 50%;">`;

    if (type == 1) {
        setTimeout(function() { 
            $('#btnFilter3').prop('disabled', false);
            $('#wrapperstartFilter3').html(`
                <div class="row">
                    <div class="col-md-7">
                        <select class="chosen-select form-control" name="filterBy3" id="filterBy3" onchange="changeMonth2()">
                            <option value="">-- Pilih Bulan --</option>
                            <option value="01">Januari</option>
                            <option value="02">Februari</option>
                            <option value="03">Maret</option>
                            <option value="04">April</option>
                            <option value="05">Mei</option>
                            <option value="06">Juni</option>
                            <option value="07">Juli</option>
                            <option value="08">Agustus</option>
                            <option value="09">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <select class="chosen-select form-control" name="yearWithMonth3" id="yearWithMonth3" onchange="changeYear2()">
                            <option value="">-- Pilih Tahun --</option>
                            <?php
                            $batas = date('Y') + 1;
                            for ($i = 2021; $i < $batas; $i++) { 
                                echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                ${loadingHide}
            `);
        }, 500);
    }
    else if (type == 2) {
        setTimeout(function() { 
            $('#btnFilter3').prop('disabled', false);
            $('#wrapperstartFilter3').html(`
                <select class="chosen-select form-control" name="filterBy3" id="filterBy3" onchange="changeYear2()">
                    <option value="">-- Pilih Tahun --</option>
                    <?php
                    $batas = date('Y') + 1;
                    for ($i = 2021; $i < $batas; $i++) { 
                        echo '<option value="'.$i.'">'.$i.'</option>';
                    }
                    ?>
                </select>
            ${loadingHide}
            `);
        }, 500);
    }
    else {
        $('#btnFilter3').prop('disabled', true);
        callingData();
        $('#wrapperstartFilter3').html(`${loadingHide2}`);
        $('#footKasMasukKeluar').hide();
    }
});

$('#formFilter3').on('submit',function(e) {
    e.preventDefault();
    var filterType3 = $('#filterType3').val();
	var filterBy3 = $('#filterBy3').val();
    var yearWithMonth3 = $('#yearWithMonth3').val();

     $('#wrapDownload3').html(`
        <a class="btn btn-success btn-sm" id="btnDownloadExcel3"> Download Excels </a>
    `);

    if ((filterBy3 == '') || (yearWithMonth3 == '')) {
        alert('Data belum dipilih !');
        return false;
    }
    else {
        $('#loading3').show();
        setTimeout(function() { 
            $('#loading3').hide();
            $("a#btnDownloadExcel3").on('click', function(e) {
                e.preventDefault();
                var query = {
                    filterType3: filterType3,
                    filterBy3: filterBy3,
                    yearWithMonth3: yearWithMonth3
                }
                var url = "{{URL::to('laporan/stockBarang')}}?" + $.param(query)
                window.location = url;
            });
            $("a#btnDownloadExcel3").css({'cursor': 'pointer', 'background-color': '#28a745', 'border-color': '#28a745'});
            // -----------------------------------------
            $("#tblDataMasukKeluarKas").dataTable().fnDestroy();
            listData(filterType3, filterBy3, yearWithMonth3);
            $('#footKasMasukKeluar').show();
         }, 500);
    }
});
</script>

@endsection