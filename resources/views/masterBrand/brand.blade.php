@extends('master')

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">Master Brand</li>
            </ul><!-- /.breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
                    <span class="input-icon">
                        <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                        <i class="ace-icon fa fa-search nav-search-icon"></i>
                    </span>
                </form>
            </div><!-- /.nav-search -->
        </div>
        
        <!-- ISI CONTENT -->
        <div class="page-content">
            
            @include('includes.setting-template')

            <div class="page-header">
                <h1>
                   Master Brand
                </h1>
            </div><!-- /.page-header -->
            <br>

            <div class="row">
                <div class="col-xs-12">

                    <a href="#modalAddData" role="button" class="btn btn-danger btn-sm btn-add" data-toggle="modal"> Add Data </a>

                    <!-- <div class="hr hr-18 dotted hr-double"></div> -->
                    <br><br>

                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table class="table table-hover" id="tblData">
                               <thead>
                                    <tr>
                                        <th width="20px">No</th>
                                        <th>Brand</th>
                                        <th width="80px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="modalAddData" class="modal" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="blue bigger">Tambah Brand</h4>
                                </div>

                                <div class="msgAlert"></div>

                                <form id="addMasterBrand"> <!-- method="post" action="saveBrand" -->
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label>Nama Brand</label>
                                                    <input type="text" name="nama_brand" id="nama_brand" class="form-control" placeholder="Nama brand ..">
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-success" value="Simpan">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- End: modal -->

                    <div id="modalEditData" class="modal" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="blue bigger">Edit Brand</h4>
                                </div>

                                <div class="msgAlert"></div>

                                <form id="editMasterBrand">
                                    {{ method_field('PUT') }}
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label>Nama Brand</label>
                                                    <input type="hidden" name="id" id="keyEdit"> 
                                                    <input type="text" name="nama_brand_edit" id="nama_brand_edit" class="form-control" placeholder="Nama brand ..">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-success" value="Simpan">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- End: modal -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->


<script>
$(document).on('ready', function() {
    $('#tblData').DataTable().ajax.reload();
});

// hide info alert
$('.msgAlert').delay(5000).fadeOut(1000);

$(document).on('click', '.btn-add, .btn-edit', function() {
    $('.msgAlert').hide();
});

$('#addMasterBrand').on('submit', function(e) {
    e.preventDefault();
    // var form = $(this).serialize();
    var formData = new FormData(this);
    // alert(formData); return false;
    var nama_brand = $("#nama_brand").val();

    if (nama_brand == '') {
        swal('Error !', 'Nama brand tidak boleh kosong !', 'error'); 
    } else {
        $.ajax({
            type: "POST",
            url : "{{ '/saveBrand' }}",
            data: formData,
            dataType: "json",
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data.s == 'success') {
                    swal('Success !', data.m, 'success'); 
                    $('#tblData').DataTable().ajax.reload();
                    $('.modal').modal('hide');
                    $('#addMasterBrand')[0].reset(); // reset form
                } 
                else {
                    swal('Error !', data.m, 'error'); 
                }
            },
            error: function(data) {
                let messages = '';
                let msg = data.responseJSON; // An array with all errors.
                $.each(msg.errors, function(key, value) {
                    messages += value;
                });
                swal('Error !', messages, 'error'); 
            }
        }); 
    }
});

$(function() {
    $('#tblData').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/getDataBrand',
        columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
                    { data: 'nama_brand', name: 'nama_brand' },
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                 ],
                "order": [[ '1', "desc" ]]
    });
 });

function editBrand(param) {
    $.ajax({
        type: "GET",
        url : "{{ '/masterBrand/edit/' }}"+param,
        dataType: "json",
        success: function(data) {
            if (data.s == 'success') {
                $('#modalEditData').modal();
                $('#keyEdit').val(param);
                $('#nama_brand_edit').val(data.data[0].nama_brand);
            } 
            else {
                alert('error');
            }
        }
    }); 
}

$('#editMasterBrand').on('submit', function(e) {
    e.preventDefault();
    // var form = $(this).serialize();
    var formData = new FormData(this);
    // alert(formData); return false;
    var nama_brand_edit = $("#nama_brand_edit").val();

    if (nama_brand_edit == '') {
        swal('Error !', 'Nama brand tidak boleh kosong !', 'error'); 
    } else {
        $.ajax({
            type: "POST",
            url : "{{ '/updateBrand' }}",
            data: formData,
            dataType: "json",
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data.s == 'success') {
                    swal('Success !', data.m, 'success'); 
                    $('.modal').modal('hide');
                    $('#tblData').DataTable().ajax.reload();
                } 
                else {
                    swal('Error !', data.m, 'error'); 
                }
            }
        }); 
    }
});

function deleteBrand(param) {
    if (confirm('Are you sure you want to delete this?')) {
        $.ajax({
            type: "GET",
            url : "{{ '/masterBrand/delete/' }}"+param,
            dataType: "json",
            success: function(data) {
                if (data.s == 'success') {
                    $('#nama_brand_edit').val(data.m);
                    $('.msgAlert').show().html('<div class="alert alert-success">'+data.m+'</div');
                    $('#tblData').DataTable().ajax.reload();
                } 
                else {
                    alert('error');
                }
            }
        }); 
    }
}
</script>

@endsection