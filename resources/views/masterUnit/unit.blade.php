@extends('master')

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>
                <li class="active">Master Satuan</li>
            </ul><!-- /.breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
                    <span class="input-icon">
                        <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
                        <i class="ace-icon fa fa-search nav-search-icon"></i>
                    </span>
                </form>
            </div><!-- /.nav-search -->
        </div>
        
        <!-- ISI CONTENT -->
        <div class="page-content">
            
            @include('includes.setting-template')

            <div class="page-header">
                <h1>
                    Master Satuan
                </h1>
            </div><!-- /.page-header -->
            <br>

            <div class="row">
                <div class="col-xs-12">

                    <a href="#modalAddData" role="button" class="btn btn-danger btn-sm btn-add" data-toggle="modal"> Add Data </a>
                    <br><br>
                    <!-- <div class="hr hr-18 dotted hr-double"></div> -->

                    <div class="row">
                        <div class="col-md-12 table-responsive">
                            <table class="table table-hover" id="tblData">
                               <thead>
                                    <tr>
                                        <th width="20px">No</th>
                                        <th>Satuan</th>
                                        <th width="80px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="modalAddData" class="modal" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="blue bigger">Tambah Satuan</h4>
                                </div>

                                <div class="msgAlert"></div>

                                <form id="addMasterUnit"> <!-- method="post" action="saveUnit" -->
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label>Nama Satuan</label>
                                                    <input type="text" name="nama_satuan" id="nama_satuan" class="form-control" placeholder="Nama satuan ..">
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-success" value="Simpan">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- End: modal -->

                    <div id="modalEditData" class="modal" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="blue bigger">Edit Satuan</h4>
                                </div>

                                <div class="msgAlert"></div>

                                <form id="editMasterUnit">
                                    {{ method_field('PUT') }}
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label>Nama Satuan</label>
                                                    <input type="hidden" name="id" id="keyEdit"> 
                                                    <input type="text" name="nama_satuan_edit" id="nama_satuan_edit" class="form-control" placeholder="Nama satuan ..">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-success" value="Simpan">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- End: modal -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->


<script>
$(document).on('ready', function() {
    $('#tblData').DataTable().ajax.reload();
});

// hide info alert
$('.msgAlert').delay(5000).fadeOut(1000);

$(document).on('click', '.btn-add, .btn-edit', function() {
    $('.msgAlert').hide();
});

$('#addMasterUnit').on('submit', function(e) {
    e.preventDefault();
    // var form = $(this).serialize();
    var formData = new FormData(this);
    // alert(formData); return false;
    var nama_satuan = $("#nama_satuan").val();

    if (nama_satuan == '') {
        swal('Error !', 'Nama unit tidak boleh kosong !', 'error'); 
    } else {
        $.ajax({
            type: "POST",
            url : "{{ '/saveUnit' }}",
            data: formData,
            dataType: "json",
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data.s == 'success') {
                    swal('Success !', data.m, 'success');
                    $('#tblData').DataTable().ajax.reload();
                    $('.modal').modal('hide');
                    $('#addMasterUnit')[0].reset(); // reset form
                } 
                else {
                    swal('Error !', data.m, 'error');
                }
            },
            error: function(data) {
                let messages = '';
                let msg = data.responseJSON; // An array with all errors.
                $.each(msg.errors, function(key, value) {
                    messages += value;
                });
                swal('Error !', messages, 'error'); 
            }
        }); 
    }
});

$(function() {
    $('#tblData').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/getDataUnit',
        columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
                    { data: 'nama_satuan', name: 'nama_satuan' },
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                 ],
                "order": [[ '1', "desc" ]]
    });
 });

function editUnit(param) {
    $.ajax({
        type: "GET",
        url : "{{ '/masterUnit/edit/' }}"+param,
        dataType: "json",
        success: function(data) {
            if (data.s == 'success') {
                $('#modalEditData').modal();
                $('#keyEdit').val(param);
                $('#nama_satuan_edit').val(data.data[0].nama_satuan);
            } 
            else {
                alert('error');
            }
        }
    }); 
}

$('#editMasterUnit').on('submit', function(e) {
    e.preventDefault();
    // var form = $(this).serialize();
    var formData = new FormData(this);
    // alert(formData); return false;
    var nama_satuan_edit = $("#nama_satuan_edit").val();

    if (nama_satuan_edit == '') {
        swal('Error !', 'Nama unit tidak boleh kosong !', 'error');
    } else {
        $.ajax({
            type: "POST",
            url : "{{ '/updateUnit' }}",
            data: formData,
            dataType: "json",
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                if (data.s == 'success') {
                    swal('Success !', data.m, 'success');
                    $('#tblData').DataTable().ajax.reload();
                    $('.modal').modal('hide');
                } 
                else {
                    swal('Error !', data.m, 'error');
                }
            }
        }); 
    }
});

function deleteUnit(param) {
    if (confirm('Are you sure you want to delete this?')) {
        $.ajax({
            type: "GET",
            url : "{{ '/masterUnit/delete/' }}"+param,
            dataType: "json",
            success: function(data) {
                if (data.s == 'success') {
                    $('#nama_satuan_edit').val(data.m);
                    $('.msgAlert').show().html('<div class="alert alert-success">'+data.m+'</div');
                    $('#tblData').DataTable().ajax.reload();
                } 
                else {
                    alert('error');
                }
            }
        }); 
    }
}
</script>

@endsection