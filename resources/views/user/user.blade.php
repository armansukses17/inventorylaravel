@extends('master')

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>

                <li>
                    <a href="#">Tables</a>
                </li>
                <li class="active">Simple &amp; Dynamic</li>
            </ul><!-- /.breadcrumb -->
        </div>
        
        <!-- ISI CONTENT -->
        <div class="page-content">
            
            @include('includes.setting-template')

            <div class="page-header">
                <h1>
                   List User
                </h1>
            </div><!-- /.page-header -->
            <br>

            <div class="row">
                <div class="col-xs-12">

                    <a href="#modalAddData" role="button" class="btn btn-danger btn-sm btn-add" data-toggle="modal"> Add Data </a>

                    <!-- <div class="hr hr-18 dotted hr-double"></div> -->
                    <br><br>

                    <div class="row">
                        <div class="col-md-12 table-responsive">

                            <div class="msgAlert"></div>

                            <table class="table table-hover" id="tblData">
                               <thead>
                                    <tr>
                                        <th width="20px">No</th>
                                        <th>Foto</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Password</th>
                                        <th>Level</th>
                                        <th>No. Hp</th>
                                        <th width="80px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="modalAddData" class="modal" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="blue bigger">Tambah User</h4>
                                </div>

                                <div class="msgAlert"></div>

                                <form id="addMasterUser" enctype="multipart/form-data"> <!-- method="post" action="saveUser" -->
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label>Nama User</label>
                                                    <input type="text" name="fullName" id="fullName" class="form-control" placeholder="Nama User ..">
                                                </div>
                                                <div class="form-group">
                                                    <label>Jenis Kelamin</label>
                                                    <!-- <input type="text" name="gender" id="gender" class="form-control" placeholder="Jenis Kelamin .."> -->
                                                    <div class="radio" style="margin-top: 0;">
                                                        <label style="padding-left: 10px;">
                                                            <input name="gender" type="radio" class="ace" value="L">
                                                            <span class="lbl"> Laki - laki</span>
                                                        </label>
                                                        <label>
                                                            <input name="gender" type="radio" class="ace" value="P">
                                                            <span class="lbl"> Perempuan</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" name="email" id="email" class="form-control" placeholder="Email ..">
                                                </div>
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="password" name="password" id="password" class="form-control" placeholder="Password ..">
                                                </div>
                                                <div class="form-group">
                                                    <label>Level</label>
                                                    <select class="form-control" id="role" name="role">
                                                        <option value="">-- Pilih --</option>
                                                        <option value="2">Admin</option>
                                                        <!-- <option value="1">Owner</option> -->
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Photo</label>
                                                    <input type="file" name="photo" id="photo" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label>No. Telp.</label>
                                                    <input type="text" name="telp" id="telp" class="form-control" placeholder="No. Telp ..">
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat</label>
                                                    <textarea class="form-control" id="address" name="address" placeholder="Alamat ..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-success" value="Simpan">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- End: modal -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<style>
    .alert-error {
        float: left;
    }
</style>

<script>
$(document).on('ready', function() {
    $('#tblData').DataTable().ajax.reload();
});

// hide info alert
$('.msgAlert').delay(5000).fadeOut(1000);

$(document).on('click', '.btn-add, .btn-edit', function() {
    $('.msgAlert').hide();
});

$('#addMasterUser').on('submit', function(e) {
    e.preventDefault();

    var formData = new FormData(this);
    var fullName = $("#fullName").val();

    $.ajax({
        type: "POST",
        url : "{{ '/saveUser' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                swal('Success !', data.m, 'success');
                $('#tblData').DataTable().ajax.reload();
                $('.modal').modal('hide');
                $('#addMasterUser')[0].reset(); // reset form
            } 
            else {
                let messages = '';
                $.each(data.errors, function(key, value) {
                    messages += '<span class="alert-error">'+value+'</span>';
                });
                toastr.error(messages);
            }
        },
        error: function(data) {
            let messages = '';
            let msg = data.responseJSON; // An array with all errors.
            $.each(msg.errors, function(key, value) {
                messages += '<span class="alert-error">'+value+'</span>';
            });
            toastr.error(messages);
        }
    }); 
});

$(function() {
    $('#tblData').DataTable({
        processing: true,
        serverSide: true,
        ajax: '/getDataUser',
        columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false }, // no autoincrement
                    { data: 'photo', name: 'photo',
                        render: function( data, type, full, meta ) {
                            return "<img src=\"/assets/files/users/" + data + "\" height=\"40\"/>";
                        }
                    },
                    { data: 'fullName', name: 'fullName' },
                    { data: 'email', name: 'email' },
                    { data: 'password', name: 'password' },
                    { data: 'level', name: 'level' },
                    { data: 'phone', name: 'phone' },
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                 ],
                "order": [[ '1', "desc" ]]
    });
 });

function deleteUser(param) {
    if (confirm('Are you sure you want to delete this?')) {
        $.ajax({
            type: "GET",
            url : "{{ '/masterUser/delete/' }}"+param,
            dataType: "json",
            success: function(data) {
                if (data.s == 'success') {
                    $('#fullName_edit').val(data.m);
                    $('.msgAlert').show().html('<div class="alert alert-success">'+data.m+'</div');
                    $('#tblData').DataTable().ajax.reload();
                } 
                else {
                    alert('error');
                }
            }
        }); 
    }
}
</script>

@endsection