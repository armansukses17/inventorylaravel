@extends('master')

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs ace-save-state" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Home</a>
                </li>

                <li>
                    <a href="{{ url('/masterUser') }}">Users</a>
                </li>
                <li class="active">Detail</li>
            </ul><!-- /.breadcrumb -->
        </div>
        
        <!-- ISI CONTENT -->
        <div class="page-content">
            
            @include('includes.setting-template')

            <div class="page-header">
                <h1>
                   Detail User
                </h1>
            </div><!-- /.page-header -->
            <br>

            <div class="row">
                <div class="col-xs-12">
                    <div id="user-profile-1" class="user-profile row">
                        <div class="col-xs-12 col-sm-3 center">
                            <div>
                                <span class="profile-picture">
                                    <img id="avatar" class="editable img-responsive editable-click editable-empty" alt="Alex's Avatar" src="{{ asset('/assets/files/users/'.$detail->photo) }}" style="display: block;">
                                </span>

                                <div class="space-4"></div>

                                <div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
                                    <div class="inline position-relative">
                                        <a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <i class="ace-icon fa fa-circle light-green"></i>
                                            &nbsp;
                                            <span class="white">{{ $detail->fullName }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-9">
                            <div class="profile-user-info profile-user-info-striped">
                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Nama Lengkap </div>
                                    <div class="profile-info-value">
                                        <span class="editable editable-click" id="fullName">{{ $detail->fullName }}</span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Jenis Kelamin </div>
                                    <div class="profile-info-value">
                                        <span class="editable editable-click" id="gender">{{ $detail->gender }}</span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Email </div>
                                    <div class="profile-info-value">
                                        <span class="editable editable-click" id="email">{{ $detail->email }}</span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Password </div>
                                    <div class="profile-info-value">
                                        <span class="editable editable-click" id="password">**********</span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Role </div>
                                    <div class="profile-info-value">
                                        <span class="editable editable-click" id="email">
                                            @if($detail->level == '1')         
                                                <span class="badge badge-info">Owner</span>        
                                            @elseif($detail->level == '2')
                                                <span class="badge badge-info">Admin</span>      
                                            @endif
                                        </span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> No. Telp. </div>
                                    <div class="profile-info-value">
                                        <span class="editable editable-click" id="email">{{ $detail->phone }}</span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Alamat </div>
                                    <div class="profile-info-value">
                                        <span class="editable editable-click" id="email">{{ $detail->address }}</span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Joined </div>
                                    <div class="profile-info-value">
                                        <span class="editable editable-click" id="signup">{{ $detail->created_at }}</span>
                                    </div>
                                </div>

                                <div class="profile-info-row">
                                    <div class="profile-info-name"> Updated At </div>
                                    <div class="profile-info-value">
                                        <span class="editable editable-click" id="signup">{{ $detail->updated_at }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="space-6"></div>

                            <div class="col-md-12">
                                <div class="pull-right">
                                    <a href="{{ url('/masterUser') }}">
                                        <button type="button" class="btn btn-sm btn-primary btn-white btn-round">
                                            <i class="icon-on-left ace-icon fa fa-arrow-left"></i>
                                            <span class="bigger-110">Back</span>
                                        </button>
                                    </a>
                                    <button type="button" class="btn btn-sm btn-primary btn-white btn-round" onclick="editUser({{ $detail->id }})">
                                        <i class="ace-icon fa fa-pencil bigger-150 middle orange2"></i>
                                        <span class="bigger-110">Edit Data</span>
                                        <i class="icon-on-right ace-icon fa fa-arrow-right"></i>
                                    </button> 
                                </div>
                            </div>
                        </div>

                        <!-- Modal edit data -->
                        <div id="modalEditData" class="modal" tabindex="-1">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="blue bigger">Edit User</h4>
                                    </div>

                                    <div class="msgAlert"></div>

                                    <form id="updateUser">
                                        {{ method_field('PUT') }}
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-12">
                                                    {{ csrf_field() }}
                                                    <div class="form-group">
                                                        <label>Nama</label>
                                                        <input type="hidden" name="id" id="keyEdit"> 
                                                        <input type="text" name="fullName_edit" id="fullName_edit" class="form-control" placeholder="Nama lengkap ..">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Jenis Kelamin</label>
                                                        <div class="radio" style="margin-top: 0;">
                                                            <label style="padding-left: 10px;">
                                                                <input name="gender_edit" type="radio" class="ace" value="L">
                                                                <span class="lbl"> Laki - laki</span>
                                                            </label>
                                                            <label>
                                                                <input name="gender_edit" type="radio" class="ace" value="P">
                                                                <span class="lbl"> Perempuan</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input type="text" name="email_edit" id="email_edit" class="form-control" placeholder="Email ..">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Photo</label><br>
                                                        <span class="upload-picture" style="position: relative; display: none; float: left; width: 100%;">
                                                            <i class="fa fa-times" id="closeUpload" title="Cancel Upload" aria-hidden="true" style="position: absolute;right: -5px;top: -7px;color: #f00;cursor: pointer;"></i>
                                                            <input type="file" name="photo_edit" id="photo_edit" class="form-control">
                                                        </span>
                                                        <span class="picture" style="position: relative; float:left;">
                                                            <i class="fa fa-times" id="closeImg" title="Change Photo" aria-hidden="true" style="position: absolute;right: -4px;top: -7px;color: #f00;cursor: pointer;"></i>
                                                            <img id="avatar" class="editable img-responsive editable-click editable-empty" alt="Alex's Avatar" src="{{ asset('/assets/files/users/'.$detail->photo) }}" width="50px">
                                                        </span>
                                                        <div class="clearfix" style="margin-bottom: 15px"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>No. Telp.</label>
                                                        <input type="text" name="telp_edit" id="telp_edit" class="form-control" placeholder="No. Telp ..">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Alamat</label>
                                                        <textarea class="form-control" id="address_edit" name="address_edit" placeholder="Alamat ..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <input type="submit" class="btn btn-success" value="Update">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- End: modal -->
                        <!-- End: Modal edit data -->
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->


<script>
function editUser(param) {
    $.ajax({
        type: "GET",
        url : "{{ '/masterUser/edit/' }}"+param,
        dataType: "json",
        success: function(data) {
            if (data.s == 'success') {
                $('#modalEditData').modal();
                $('#keyEdit').val(param);
                $('#fullName_edit').val(data.data[0].fullName);

                if (data.data[0].gender == 'L') {
                    $(':radio[name=gender_edit][value="L"]').prop('checked', true);
                }
                else {
                    $(':radio[name=gender_edit][value="P"]').prop('checked', true);
                }

                $('#email_edit').val(data.data[0].email);
                $('#telp_edit').val(data.data[0].phone);
                $('#address_edit').val(data.data[0].address);
            }
            else {
                alert('error');
            }
        }
    }); 
}

$('#updateUser').on('submit', function(e) {
    e.preventDefault();
    // var form = $(this).serialize();
    var formData = new FormData(this);

    $.ajax({
        type: "POST",
        url : "{{ '/updateUser' }}",
        data: formData,
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
            if (data.s == 'success') {
                // swal('Success !', data.m, 'success');
                swal({
                    title: "Success !",
                    text: data.m,
                    type: "success",
                    timer: 2000
                });
                window.setTimeout(function(){ 
                    location.reload();
                } ,2000);
            } 
            else {
                swal('Error !', data.m, 'error');
            }
        },
        error: function(data) {
            let messages = '';
            let msg = data.responseJSON; // An array with all errors.
            $.each(msg.errors, function(key, value) {
                messages += '<span class="alert-error">'+value+'</span>';
            });
            toastr.error(messages);
        }
    }); 
});

$('#closeImg').on('click', function() {
    $('.picture').hide();
    $('.upload-picture').show();
});

$('#closeUpload').on('click', function() {
    $('.picture').show();
    $('.upload-picture').hide();
});
</script>

@endsection