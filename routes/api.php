<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

date_default_timezone_set('Asia/Jakarta');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('user', 'RestApiUser@index');
Route::get('user/show/{id}', 'RestApiUser@show');
Route::post('user/create', 'RestApiUser@create');
Route::put('user/update/{id}', 'RestApiUser@update');
Route::delete('user/delete/{id}', 'RestApiUser@destroy');
