<?php

// use Illuminate\Support\Facades\Auth; // aaaaaaaaaaaaaaaaaaaaaa
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();
// Login
Route::get('logins', 'AuthController@index')->name('login');
Route::post('postLogin', 'AuthController@postLogin');
Route::get('logout', 'AuthController@logout');

// acess admin & owner
Route::group(['middleware' => ['auth', 'checkRolle: 1, 2']], function() { 
    // Unit ---------------
    Route::get('masterUnit', 'MasterUnit@index'); // ->middleware('auth');
    Route::post('saveUnit', 'MasterUnit@store');
    Route::get('getDataUnit', 'MasterUnit@getDataUnit');
    Route::get('masterUnit/edit/{id}', 'MasterUnit@edit');
    Route::put('updateUnit', 'MasterUnit@update');
    Route::get('masterUnit/delete/{id}', 'MasterUnit@destroy');
    // Brand ---------------
    Route::get('masterBrand', 'MasterBrand@index');
    Route::post('saveBrand', 'MasterBrand@store');
    Route::get('getDataBrand', 'MasterBrand@getDataBrand');
    Route::get('masterBrand/edit/{id}', 'MasterBrand@edit');
    Route::put('updateBrand', 'MasterBrand@update');
    Route::get('masterBrand/delete/{id}', 'MasterBrand@destroy');
    // Kategori ---------------
    Route::get('masterKategori', 'MasterKategori@index');
    Route::post('saveKategori', 'MasterKategori@store');
    Route::get('getDataKategori', 'MasterKategori@getDataKategori');
    Route::get('masterKategori/edit/{id}', 'MasterKategori@edit');
    Route::put('updateKategori', 'MasterKategori@update');
    Route::get('masterKategori/delete/{id}', 'MasterKategori@destroy');
    // Supplier ---------------
    Route::get('masterSupplier', 'MasterSupplier@index');
    Route::post('saveSupplier', 'MasterSupplier@store');
    Route::get('getDataSupplier', 'MasterSupplier@getDataSupplier');
    Route::get('masterSupplier/edit/{id}', 'MasterSupplier@edit');
    Route::put('updateSupplier', 'MasterSupplier@update');
    Route::get('masterSupplier/delete/{id}', 'MasterSupplier@destroy');
     // Customer ---------------
    Route::get('masterCustomer', 'MasterCustomer@index');
    Route::post('saveCustomer', 'MasterCustomer@store');
    Route::get('getDataCustomer', 'MasterCustomer@getDataCustomer');
    Route::get('masterCustomer/edit/{id}', 'MasterCustomer@edit');
    Route::put('updateCustomer', 'MasterCustomer@update');
    Route::get('masterCustomer/delete/{id}', 'MasterCustomer@destroy');
    // Data Barang ---------------
    Route::get('dataBarang', 'Barang@index');
    Route::post('saveBarang', 'Barang@store');
    Route::get('getDataBarang', 'Barang@getDataBarang');
    Route::get('barang/edit/{id}', 'Barang@edit');
    Route::put('updateBarang', 'Barang@update');
    Route::get('barang/delete/{id}', 'Barang@destroy');
    Route::get('checkStockInWarehouse/{id}', 'Barang@checkStockInWarehouse');
    Route::get('checkStockInWarehouse2/{id}', 'Barang@checkStockInWarehouse2');
    // Autocomplete barang
    Route::post('search/product', 'Barang@getAutocompleteProduct');
    Route::post('search/productReady', 'Barang@getAutocompleteProductReadyStock');
    Route::put('addStokBarang', 'Barang@addStokBarang');
    // Barang Masuk ---------------
    Route::get('barangMasuk', 'BarangMasuk@index');
    Route::post('saveBarangMasuk', 'BarangMasuk@store');
    Route::get('getDataBarangMasuk', 'BarangMasuk@getDataBarangMasuk');
    Route::get('barangMasuk/edit/{id}', 'BarangMasuk@edit');
    Route::put('updateBarangMasuk', 'BarangMasuk@update');
    Route::get('barangMasuk/delete/{id}', 'BarangMasuk@destroy');
    // Barang Keluar ---------------
    Route::get('barangKeluar', 'BarangKeluar@index');
    Route::post('saveBarangKeluar', 'BarangKeluar@store');
    Route::get('getDataBarangKeluar', 'BarangKeluar@getDataBarangKeluar');
    Route::get('barangKeluar/edit/{id}', 'BarangKeluar@edit');
    Route::put('updateBarangKeluar', 'BarangKeluar@update');
    Route::get('barangKeluar/delete/{id}', 'BarangKeluar@destroy');
    // Faktur
    Route::get('getFakturOut', 'BarangKeluar@generateFaktur');

    // user ---------------
    Route::get('masterUser', 'MasterUser@index');
    Route::post('saveUser', 'MasterUser@store');
    Route::get('getDataUser', 'MasterUser@getDataUser');
    Route::get('masterUser/detail/{id}', 'MasterUser@show');
    Route::get('masterUser/edit/{id}', 'MasterUser@edit');
    Route::put('updateUser', 'MasterUser@update');
    Route::get('masterUser/delete/{id}', 'MasterUser@destroy');
    // homepage / dashboard
    Route::get('', 'HomeController@index');
    Route::get('dashboard', 'HomeController@index');
    // Laporan stok ---------------
    Route::get('lapStokBarang', 'LaporanStok@pageAllReport');
    Route::get('getLapStok', 'LaporanStok@getLapAllStok');
    Route::get('testDownlod', 'LaporanStok@testDownlod'); 
    Route::get('laporan/stockBarang', 'LaporanStok@LaporanStockBarang'); 
});

